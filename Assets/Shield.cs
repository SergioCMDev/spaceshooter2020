﻿using System;
using System.Collections;
using System.Collections.Generic;
using Code;
using Code.Msile;
using Code.Obstacle;
using UnityEngine;

public class Shield : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D other)
    {
        Backgound background = other.GetComponent<Backgound>();
        Missile missile = other.GetComponent<Missile>();
        Mine mine = other.GetComponent<Mine>();
        Bullet bullet = other.GetComponent<Bullet>();
        Enemy enemy = other.GetComponent<Enemy>();
        Meteor meteor = other.GetComponent<Meteor>();
        if (background)
        {
            return;
        }

        if (bullet && bullet.BulletType == BulletTypes.FromEnemy)
        {
            bullet.Dispose();
        }
        else if (mine)
        {
            mine.KilledByPlayer = true;
            mine.MineExplode();
        }
        else if (enemy)
        {
            enemy.DestroyEnemy();
        }
        else if (meteor)
        {
            meteor.KilledByPlayer = true;
            meteor.Dispose();
        }else if (missile)
        {
            missile.Destroy();
        }
    }
}