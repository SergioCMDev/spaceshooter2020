﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Code.Utils;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;
using Zenject;

public class AudioManager : MonoBehaviour, IInitializable
{
    private AudioSource _backgroundMusic;
    private AudioSource _bombExplosion1;
    private AudioSource _bombExplosion2;
    private AudioSource _laserEnemyShot;
    private AudioSource _laserPlayerShot;
    private AudioSource _pickUpPowerUp;
    private AudioSource _missileShot;
    private AudioSource _uIClick;
    private AudioSource _meteorHitsPlayer;
    private AudioSource _missileHitsPlayer;
    private AudioSource _playerHitsEnemy;

    [SerializeField] private AudioClip _backgroundMusicAudioClip;
    [SerializeField] private AudioClip _bombExplosion1AudioClip;
    [SerializeField] private AudioClip _bombExplosion2AudioClip;
    [SerializeField] private AudioClip _laserEnemyShotAudioClip;
    [SerializeField] private AudioClip _laserPlayerShotAudioClip;
    [SerializeField] private AudioClip _pickUpPowerUpAudioClip;
    [SerializeField] private AudioClip _missileShotAudioClip;
    [SerializeField] private AudioClip _uIClickAudioClip;

    [SerializeField] private AudioClip _meteorHitsPlayerAudioClip;
    [SerializeField] private AudioClip _playerHitsEnemyAudioClip;
    [SerializeField] private AudioMixer _audioMixer;
    private List<AudioClip> _audioClips = new List<AudioClip>();

    private void Awake()
    {
#if !UNITY_WEBGL
        LoadAudioAssets();
#endif
        _backgroundMusic = gameObject.AddComponent<AudioSource>();
        _backgroundMusic.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("Background")[0];
        _backgroundMusic.clip = _backgroundMusicAudioClip;
        _backgroundMusic.loop = true;

        _laserPlayerShot = gameObject.AddComponent<AudioSource>();
        _laserPlayerShot.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("Effects")[0];

        _laserEnemyShot = gameObject.AddComponent<AudioSource>();
        _laserEnemyShot.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("Effects")[0];


        _bombExplosion1 = gameObject.AddComponent<AudioSource>();
        _bombExplosion1.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("Effects")[0];

        _bombExplosion2 = gameObject.AddComponent<AudioSource>();
        _bombExplosion2.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("Effects")[0];

        _pickUpPowerUp = gameObject.AddComponent<AudioSource>();
        _pickUpPowerUp.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("Effects")[0];

        _missileShot = gameObject.AddComponent<AudioSource>();
        _missileShot.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("Effects")[0];

        _uIClick = gameObject.AddComponent<AudioSource>();
        _uIClick.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("Effects")[0];
        
        _meteorHitsPlayer = gameObject.AddComponent<AudioSource>();
        _meteorHitsPlayer.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("Effects")[0];

        _playerHitsEnemy = gameObject.AddComponent<AudioSource>();
        _playerHitsEnemy.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("Effects")[0];

#if UNITY_WEBGL
        _laserPlayerShot.clip = _laserPlayerShotAudioClip;
        _laserEnemyShot.clip = _laserEnemyShotAudioClip;
        _bombExplosion1.clip = _bombExplosion1AudioClip;
        _bombExplosion2.clip = _bombExplosion2AudioClip;
        _pickUpPowerUp.clip = _pickUpPowerUpAudioClip;
        _missileShot.clip = _missileShotAudioClip;
        _uIClick.clip = _uIClickAudioClip;
        _meteorHitsPlayer.clip = _meteorHitsPlayerAudioClip;
        _playerHitsEnemy.clip = _playerHitsEnemyAudioClip;

        
#endif
    }

    public async Task LoadAudioAssets()
    {
        IList<IResourceLocation> loadedLocations = new List<IResourceLocation>();
        IList<AudioClip> obj = new List<AudioClip>();

        await LoadAddressablesLocations.LoadLocations<AudioClip>("AudioClips", loadedLocations, null);
        await LoadAddressablesLocations.GetAssetsOnLocations(loadedLocations, obj, LoadAudioEnds);
    }

    private void LoadAudioEnds(AsyncOperationHandle<IList<AudioClip>> obj)
    {
        foreach (var audioClip in obj.Result)
        {
            _audioClips.Add(audioClip);
            switch (audioClip.name)
            {
                case "BackgroundMusic":
                    _backgroundMusic.clip = audioClip;
                    break;
                case "LaserPlayerShot":
                    _laserPlayerShot.clip = audioClip;
                    break;
                case "LaserEnemyShot":
                    _laserEnemyShot.clip = audioClip;
                    break;
                case "BombExplosion1":
                    _bombExplosion1.clip = audioClip;
                    break;
                case "BombExplosion2":
                    _bombExplosion2.clip = audioClip;
                    break;
                case "PowerUpPickUp":
                    _pickUpPowerUp.clip = audioClip;
                    break;
                case "MissileShot":
                    _missileShot.clip = audioClip;
                    break;
                case "UIClick":
                    _uIClick.clip = audioClip;
                    break;
                case "MeteorHit":
                    _meteorHitsPlayer.clip = audioClip;
                    break;
                case "PlayerHitEnemy":
                    _playerHitsEnemy.clip = audioClip;
                    break;
            }

            // Debug.Log(audioClip.name);
        }
    }


    public void Initialize()
    {
    }

    public void SetStatusBackgroundMusic(bool enableSound)
    {
        if (enableSound)
        {
            _backgroundMusic.Play();
            return;
        }

        _backgroundMusic.Stop();
    }

    public void PlayPlayerLaserSoundEffect()
    {
        _laserPlayerShot.Play();
    }

    public void PlayEnemyLaserSoundEffect()
    {
        _laserEnemyShot.Play();
    }

    public void PlayEnemyExplodingSoundEffect()
    {
        _bombExplosion1.Play();
    }

    public void PlayPlayerExplodingSoundEffect()
    {
        _bombExplosion1.Play();
    }

    public void PlayPickingPowerUpSoundEffect()
    {
        _pickUpPowerUp.Play();
    }

    public void PlayMineExplodingSoundEffect()
    {
        _meteorHitsPlayer.Play();
    }

    public void PlayMissileExplodingSoundEffect()
    {
        _bombExplosion2.Play();
    }

    public void PlayMissileShotSoundEffect()
    {
        _missileShot.Play();
    }

    public void PlayUiClickSoundEffect()
    {
        _uIClick.Play();
    }

    public void PlayMeteorHitSoundEffect()
    {
        _meteorHitsPlayer.Play();
    }

    public void PlayPlayerHitsEnemySoundEffect()
    {
        _playerHitsEnemy.Play();
    }
}