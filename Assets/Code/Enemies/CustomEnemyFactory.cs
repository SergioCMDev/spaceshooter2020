﻿using Zenject;

namespace Code
{
    public class CustomEnemyFactory : IFactory<EnemyInfo, IShot, Enemy>
    {
        [Inject] private Enemy1.Factory _factory1;
        [Inject] private Enemy2.Factory _factory2;
        [Inject] private Enemy3.Factory _factory3;


        public Enemy Create(EnemyInfo enemyInfo, IShot shotStrategy)
        {
            switch (enemyInfo.EnemyType)
            {
                case EnemyType.Type1:
                    return _factory1.Create(enemyInfo, shotStrategy);
                case EnemyType.Type2:
                    return _factory2.Create(enemyInfo,shotStrategy);
                case EnemyType.Type3:
                    return _factory3.Create(enemyInfo, shotStrategy);
                default:
                    return _factory1.Create(enemyInfo, shotStrategy);
            }
        }

        public class EnemyFactory : PlaceholderFactory<EnemyInfo, IShot, Enemy>
        {
        }
    }
}