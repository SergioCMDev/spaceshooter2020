﻿using Code;
using Code.Msile;
using Code.Signals;
using UnityEngine;
using Zenject;


public class Enemy2 : Enemy, IPoolable<EnemyInfo, IShot, IMemoryPool>

{
    [SerializeField] private float _maxDistanceToCheckBullets = 10;
    [SerializeField] private float _movementValue = 2;
    [SerializeField] private SpriteController _spriteController;

    IMemoryPool _pool;

    private void Start()
    {
        _spriteController.OnDeadAnimationEnds += DeadDeadAnimationEnds;

        InvokeRepeating("Shot", TimeToStartShooting, TimeBetweenShots);
    }

    private void OnDestroy()
    {
        _spriteController.OnDeadAnimationEnds -= DeadDeadAnimationEnds;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Bullet bullet = other.GetComponent<Bullet>();
        if (bullet && bullet.BulletType == BulletTypes.FromPlayer)
        {
            EnemyGetsHit(bullet);
        }
    }

    private void EnemyGetsHit(Bullet bullet)
    {
        UpdateEnemyLife(bullet.Damage);
    }

    private void DeadDeadAnimationEnds()
    {
        Dispose();
    }

    private void Update()
    {
        transform.Translate(Direction * Time.deltaTime * MovementSpeed);
        if (Time.realtimeSinceStartup - StartTime > LifeTime)
        {
            _signalBus.Fire(new EnemyDestroyedByTimeSignal(){ Enemy = this});

            Dispose();
        }

        if (IsFacingBullets())
        {
            EvadeBullets();
        }
    }


    private bool IsFacingBullets()
    {
        Vector2 position = new Vector2(transform.position.x, transform.position.y);
        RaycastHit2D hit = Physics2D.BoxCast(position + Vector2.down * _maxDistanceToCheckBullets, transform.localScale,
            0, transform.forward, 0);

        if (hit.collider != null)
        {
            Bullet bullet = hit.collider.GetComponent<Bullet>();
            if (bullet && bullet.BulletType == BulletTypes.FromPlayer)
            {
                return true;
            }
        }

        return false;
    }

    private void EvadeBullets()
    {
        RaycastHit2D hitRight =
            Physics2D.Raycast(transform.position + Vector3.right * 1.5f, Vector2.right * _movementValue);
        RaycastHit2D hitLeft =
            Physics2D.Raycast(transform.position + Vector3.left * 1.5f, -Vector2.left * _movementValue);
        Debug.Log("Se mueve enemigo");
        Vector3 newPositionLeft = transform.position + Vector3.left * _movementValue;
        Vector3 newPositionRight = transform.position + Vector3.right * _movementValue;
        if (hitRight.collider == null && newPositionRight.x < 20)
        {
            transform.position = newPositionRight;
        }
        else if (hitLeft.collider == null && newPositionRight.x > -20)
        {
            transform.position = newPositionLeft;
        }
    }

    private void OnDrawGizmos()
    {
        Debug.DrawLine(transform.position + Vector3.down * 2.0f, transform.position + -transform.up * 5.0f,
            Color.green);

        Debug.DrawLine(transform.position + Vector3.right * 1.5f, transform.position + transform.right * 5.0f,
            Color.blue);

        Debug.DrawLine(transform.position + Vector3.left * 1.5f, transform.position + -transform.right * 5.0f,
            Color.blue);

        Gizmos.color = Color.red;

        Gizmos.DrawWireCube(transform.position + Vector3.down * _maxDistanceToCheckBullets,
            transform.localScale);
    }


    public void OnSpawned(EnemyInfo enemyInfo, IShot shotStrategy, IMemoryPool pool)
    {
        _pool = pool;
        enemyInfo.ShotStrategy = shotStrategy;
        enemyInfo.ScoreForKill = 20;
        KilledByPlayer = false;

        base.OnSpawned(enemyInfo, pool);
    }

    public void Shot()
    {
        if (!KilledByPlayer && gameObject.activeInHierarchy)
        {
            AudioManager.PlayEnemyLaserSoundEffect();

            float bulletLifeTime = 4;
            ProjectileInfo projectileInfo = new ProjectileInfo()
            {
                MovementSpeed = BulletSpeed,
                Damage = BulletDamage,
                Direction = Vector2.down,
                LifeTime = bulletLifeTime,
                BulletTypes = BulletTypes.FromEnemy
            };
            ShotStrategy.Shot(projectileInfo, _shotPoints);
        }
    }

    public class Factory : PlaceholderFactory<EnemyInfo, IShot, Enemy2>
    {
    }
}