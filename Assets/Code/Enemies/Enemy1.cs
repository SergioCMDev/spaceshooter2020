﻿using Code;
using Code.Msile;
using Code.Signals;
using UnityEngine;
using Zenject;


public class Enemy1 : Enemy, IPoolable<EnemyInfo, IShot, IMemoryPool>
{
    IMemoryPool _pool;
    [SerializeField] private SpriteController _spriteController;

    private void Start()
    {
        _spriteController.OnDeadAnimationEnds += DeadDeadAnimationEnds;
        InvokeRepeating("Shot", TimeToStartShooting, TimeBetweenShots);
    }

    private void OnDestroy()
    {
        _spriteController.OnDeadAnimationEnds -= DeadDeadAnimationEnds;
    }

    private void DeadDeadAnimationEnds()    {
        Dispose();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Bullet bullet = other.GetComponent<Bullet>();
        if (bullet && bullet.BulletType == BulletTypes.FromPlayer)
        {
            EnemyGetsHit(bullet);
        }
    }

    private void EnemyGetsHit(Bullet bullet)
    {
        UpdateEnemyLife(bullet.Damage);
    }

    private void Update()
    {
        transform.Translate(Direction * Time.deltaTime * MovementSpeed);
        if (Time.realtimeSinceStartup - StartTime > LifeTime)
        {
            _signalBus.Fire(new EnemyDestroyedByTimeSignal(){ Enemy = this});
            Dispose();
        }
    }


    public void OnSpawned(EnemyInfo enemyInfo, IShot shotStrategy, IMemoryPool pool)
    {
        _pool = pool;
        enemyInfo.ShotStrategy = shotStrategy;
        KilledByPlayer = false;

        enemyInfo.ScoreForKill = 10;
        base.OnSpawned(enemyInfo, pool);
    }

    public void Shot()
    {
        if (!KilledByPlayer && gameObject.activeInHierarchy)
        {
            float bulletLifeTime = 4;
            AudioManager.PlayEnemyLaserSoundEffect();
            ProjectileInfo projectileInfo = new ProjectileInfo()
            {
                MovementSpeed = BulletSpeed,
                Damage = BulletDamage,
                Direction = Vector2.down,
                LifeTime = bulletLifeTime,
                BulletTypes = BulletTypes.FromEnemy, 
                Rotation = transform.rotation
            };
            ShotStrategy.Shot(projectileInfo, _shotPoints);
        }
    }

    public class Factory : PlaceholderFactory<EnemyInfo, IShot, Enemy1>
    {
    }
}