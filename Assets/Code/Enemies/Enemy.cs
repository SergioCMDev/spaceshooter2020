﻿using System;
using System.Collections.Generic;
using Code;
using Code.Signals;
using UnityEngine;
using Zenject;

public struct EnemyInfo
{
    public float MovementSpeed;
    public float LifeTime;
    public int Life;
    public int ProjectileDamage;
    public int TimeBetweenShots;
    public int TimeToStartShooting;
    public int ScoreForKill;
    public EnemyType EnemyType;
    public Vector3 PositionOnDead;
    public Vector3 Direction;
    public float BulletSpeed;
    public Transform Player;
    public IShot ShotStrategy;
}

public enum EnemyType
{
    Type1,
    Type2,
    Type3
}


public abstract class Enemy : MonoBehaviour, IDisposable
{
    [Inject] protected SignalBus _signalBus;
    [SerializeField] protected List<Transform> _shotPoints;
    protected IShot ShotStrategy;
    [SerializeField] private Animator _animator;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    protected Sprite Sprite;
    [Inject] private AudioManager _audioManager;

    private IMemoryPool _pool;
    private bool _destroyed;
    protected EnemyType Type;
    protected float MovementSpeed;
    protected float StartTime;
    protected float BulletDamage;
    protected float Life;
    protected float TimeToStartShooting;
    protected float LifeTime;
    protected float TimeBetweenShots;
    public float BulletSpeed;
    protected EnemyInfo EnemyInfo;
    protected bool KilledByPlayer;
    protected Vector3 Direction;
    private static readonly int Explode = Animator.StringToHash("Explode");

    protected AudioManager AudioManager
    {
        get => _audioManager;
    }

    public bool Destroyed
    {
        get => _destroyed;
    }

    private void Start()
    {
        InvokeRepeating("Shot", TimeBetweenShots, TimeBetweenShots);
    }

    public void DestroyEnemy()
    {
        _animator.SetTrigger(Explode);
        AudioManager.PlayEnemyExplodingSoundEffect();
        EnemyInfo.PositionOnDead = transform.position;
        _destroyed = true;
        KilledByPlayer = true;
        _signalBus.Fire(new EnemyDestroyedByPlayerSignal() {EnemyInfo = EnemyInfo});
    }

    protected void UpdateEnemyLife(float damage)
    {
        Life -= damage;
        if (Life <= 0 && !KilledByPlayer)
        {
            DestroyEnemy();
        }
    }

    public void OnDespawned()
    {
        _pool = null;
        gameObject.SetActive(false);
    }

    public void OnSpawned(EnemyInfo enemyInfo, IMemoryPool pool)
    {
        _pool = pool;
        EnemyInfo = enemyInfo;
        Type = enemyInfo.EnemyType;
        BulletDamage = enemyInfo.ProjectileDamage;
        MovementSpeed = enemyInfo.MovementSpeed;
        LifeTime = enemyInfo.LifeTime;
        TimeBetweenShots = enemyInfo.TimeBetweenShots;
        Life = enemyInfo.Life;
        KilledByPlayer = false;
        StartTime = Time.realtimeSinceStartup;
        TimeToStartShooting = enemyInfo.TimeToStartShooting;
        Direction = enemyInfo.Direction;
        BulletSpeed = enemyInfo.BulletSpeed;
        ShotStrategy = enemyInfo.ShotStrategy;
        Sprite = _spriteRenderer.sprite;
        _destroyed = false;
    }

    public void Dispose()
    {
        _spriteRenderer.sprite = Sprite;

        _pool?.Despawn(this);
    }
}