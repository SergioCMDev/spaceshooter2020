﻿using System;
using UnityEngine;

public class SpriteController : MonoBehaviour
{
    public event Action OnDeadAnimationEnds = delegate { };

    //Used by Animator

    public void ExplodeAnimationEnds()
    {
        OnDeadAnimationEnds.Invoke();
    }
}