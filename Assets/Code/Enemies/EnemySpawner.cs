﻿using System;
using System.Collections.Generic;
using Code;
using Code.Signals;
using Code.Utils;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    [Inject] private readonly SignalBus _signalBus;
    [Inject] private ILevelInfoModel _levelInfoModel;
    [SerializeField] private List<RectangleToSpawn> positionsToSpawn;
    [SerializeField] private bool _spawnEnemy1;
    [SerializeField] private bool _spawnEnemy2;
    [SerializeField] private bool _spawnEnemy3;
    [SerializeField] private Transform _player;
    [Inject] private IEnemyModel _enemyModel;
    [Inject] private ShotSeveralBullets.Factory _factoryBullets;
    [Inject] private ShotMissile.Factory _factoryShotMissile;
    [Inject] private GameSettingsInstaller _commonSettings;
    [Inject] private CustomEnemyFactory.EnemyFactory _enemyFactory;

    private float _startTime;
    private int _enemiesSpawned;
    
    private ShotSeveralBullets _shotStrategyBullets;
    private ShotMissile _shotStrategyMissile;

    void Start()
    {
        _startTime = Time.realtimeSinceStartup;
        _signalBus.Subscribe<EnemyDestroyedByPlayerSignal>(EnemyKilledByPlayer);
        _signalBus.Subscribe<EnemyDestroyedByTimeSignal>(EnemyDestroyedByTime);

        _shotStrategyMissile = _factoryShotMissile.Create();
        _shotStrategyBullets = _factoryBullets.Create();
    }

    private void OnDestroy()
    {
        _signalBus?.TryUnsubscribe<EnemyDestroyedByPlayerSignal>(EnemyKilledByPlayer);
        _signalBus?.TryUnsubscribe<EnemyDestroyedByPlayerSignal>(EnemyKilledByPlayer);
    }

    private void EnemyKilledByPlayer(EnemyDestroyedByPlayerSignal signal)
    {
        float random = Random.Range(0, 100);
        _levelInfoModel.Score += signal.EnemyInfo.ScoreForKill;
        _levelInfoModel.EnemiesDefeated++;
        _signalBus.Fire(new UpdateGameInfoPointsSignal());
        if (random < _commonSettings.Enemy.PercentageToGeneratePowerUps)
        {
            _signalBus.Fire(new SpawnNewPowerUpSignal() {Position = signal.EnemyInfo.PositionOnDead});
        }
        _enemyModel.Enemies.Remove(signal.Enemy);

        _enemiesSpawned--;
    }
    
    private void EnemyDestroyedByTime(EnemyDestroyedByTimeSignal signal)
    {
        _enemyModel.Enemies.Remove(signal.Enemy);

        _enemiesSpawned--;
    }


    void Update()
    {
        if (_enemiesSpawned < _commonSettings.Enemy.MaxEnemiesToSpawn && Time.realtimeSinceStartup - _startTime > _commonSettings.Enemy.TimeToSpawnEnemy && !Utils.IsGamePaused())
        {
            SpawnEnemy();
            _startTime = Time.realtimeSinceStartup;
        }
    }

    private void SpawnEnemy()
    {
        int enemyRandom = Random.Range(0, 100);
        Enemy enemy;
        EnemyInfo enemyInfo = new EnemyInfo();

        enemyInfo.Direction = Vector3.down;

        int randomPositionToSpawn = Random.Range(0, 100);
        //We only have two positions to spawn, in case we had more we should change this. Random gives the same position 90% more or less
        if (randomPositionToSpawn < 50)
        {
            randomPositionToSpawn = 0;
        }
        else
        {
            randomPositionToSpawn = 1;
        }

        if (enemyRandom >= _commonSettings.Enemy.MinProbabiltyToSpawnEnemy1 &&
            enemyRandom <= _commonSettings.Enemy.MaxProbabiltyToSpawnEnemy1 && _spawnEnemy1)
        {
            enemyInfo.MovementSpeed = _commonSettings.Enemy.Enemy1Settings.MovementSpeedBase;
            enemyInfo.Life = _commonSettings.Enemy.Enemy1Settings.Life;
            enemyInfo.ProjectileDamage = _commonSettings.Enemy.Enemy1Settings.BulletDamage;
            enemyInfo.BulletSpeed = _commonSettings.Enemy.Enemy1Settings.BulletSpeed;
            enemyInfo.LifeTime = _commonSettings.Enemy.Enemy1Settings.LifeTime;
            enemyInfo.TimeBetweenShots = _commonSettings.Enemy.Enemy1Settings.TimeBetweenShots;
            enemyInfo.TimeToStartShooting = _commonSettings.Enemy.Enemy1Settings.TimeToStartShooting;
            enemyInfo.ScoreForKill = _commonSettings.Enemy.Enemy1Settings.ScoreForDestroyEnemy;

            enemyInfo.EnemyType = EnemyType.Type1;

            enemy = _enemyFactory.Create(enemyInfo, _shotStrategyBullets);
            SetCreatedEnemyInfo(enemy, randomPositionToSpawn);
        }
        else if (enemyRandom > _commonSettings.Enemy.MinProbabiltyToSpawnEnemy2 &&
                 enemyRandom < _commonSettings.Enemy.MaxProbabiltyToSpawnEnemy2 && _spawnEnemy2)
        {
            enemyInfo.EnemyType = EnemyType.Type2;
            enemyInfo.MovementSpeed = _commonSettings.Enemy.Enemy2Settings.MovementSpeedBase;
            enemyInfo.TimeBetweenShots = _commonSettings.Enemy.Enemy2Settings.TimeBetweenShots;
            enemyInfo.BulletSpeed = _commonSettings.Enemy.Enemy2Settings.BulletSpeed;
            enemyInfo.ProjectileDamage = _commonSettings.Enemy.Enemy2Settings.BulletDamage;
            enemyInfo.LifeTime = _commonSettings.Enemy.Enemy2Settings.LifeTime;
            enemyInfo.TimeBetweenShots = _commonSettings.Enemy.Enemy2Settings.TimeBetweenShots;
            enemyInfo.TimeToStartShooting = _commonSettings.Enemy.Enemy2Settings.TimeToStartShooting;
            enemyInfo.ScoreForKill = _commonSettings.Enemy.Enemy2Settings.ScoreForDestroyEnemy;

            enemyInfo.Life = _commonSettings.Enemy.Enemy2Settings.Life;

            enemy = _enemyFactory.Create(enemyInfo, _shotStrategyBullets);
            SetCreatedEnemyInfo(enemy, randomPositionToSpawn);
        }
        else if (enemyRandom > _commonSettings.Enemy.MinProbabiltyToSpawnEnemy3 &&
                 enemyRandom < _commonSettings.Enemy.MaxProbabiltyToSpawnEnemy3 && _spawnEnemy3)
        {
            enemyInfo.MovementSpeed = _commonSettings.Enemy.Enemy3Settings.MovementSpeedBase;
            enemyInfo.Life = _commonSettings.Enemy.Enemy3Settings.Life;
            enemyInfo.LifeTime = _commonSettings.Enemy.Enemy3Settings.LifeTime;
            enemyInfo.TimeBetweenShots = _commonSettings.Enemy.Enemy3Settings.TimeBetweenShots;
            enemyInfo.TimeToStartShooting = _commonSettings.Enemy.Enemy3Settings.TimeToStartShooting;
            enemyInfo.ScoreForKill = _commonSettings.Enemy.Enemy3Settings.ScoreForDestroyEnemy;

            enemyInfo.Player = _player;
            enemyInfo.EnemyType = EnemyType.Type3;
            enemy = _enemyFactory.Create(enemyInfo, _shotStrategyMissile);
            SetCreatedEnemyInfo(enemy, randomPositionToSpawn);
        }
    }

    private void SetCreatedEnemyInfo(Enemy enemy, int randomPositionToSpawn)
    {
        _enemyModel.Enemies.Add(enemy);
        Vector2 newPosition = Utils.GetRandomPositionToSpawn(positionsToSpawn[randomPositionToSpawn]);
        enemy.transform.position = newPosition;
        _enemiesSpawned++;
    }

    private void OnDrawGizmos()
    {
        for (int i = 0; i < positionsToSpawn.Count; i++)
        {
            Vector2 vector1 = new Vector2(positionsToSpawn[i].MinPosition.x, positionsToSpawn[i].MinPosition.y);
            Vector2 vector2 = new Vector2(positionsToSpawn[i].MinPosition.x, positionsToSpawn[i].MaxPosition.y);
            Vector2 vector3 = new Vector2(positionsToSpawn[i].MaxPosition.x, positionsToSpawn[i].MaxPosition.y);
            Vector2 vector4 = new Vector2(positionsToSpawn[i].MaxPosition.x, positionsToSpawn[i].MinPosition.y);

            Debug.DrawLine(vector1, vector2, Color.green);
            Debug.DrawLine(vector2, vector3, Color.green);
            Debug.DrawLine(vector3, vector4, Color.green);
            Debug.DrawLine(vector4, vector1, Color.green);
        }
    }

    public void Reset()
    {
        foreach (var enemy in _enemyModel.Enemies)
        {
            enemy.Dispose();
        }

        _enemyModel.Enemies.Clear();
    }
}

public interface IEnemyModel
{
    List<Enemy> Enemies { get; }
}

public class EnemyModel : IEnemyModel, IInitializable
{
    public List<Enemy> Enemies { get; set; }

    public void Initialize()
    {
        Enemies = new List<Enemy>();
    }
}