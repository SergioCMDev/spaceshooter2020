﻿using Code;
using Code.Msile;
using Code.Signals;
using UnityEngine;
using Zenject;


public class Enemy3 : Enemy, IPoolable<EnemyInfo, IShot, IMemoryPool>
{
    [SerializeField] private SpriteController _spriteController;
    [Inject] private GameSettingsInstaller _commonSettings;


    private void Start()
    {
        _spriteController.OnDeadAnimationEnds += DeadDeadAnimationEnds;

        InvokeRepeating("Shot", TimeToStartShooting, TimeBetweenShots);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Bullet bullet = other.GetComponent<Bullet>();
        if (bullet && bullet.BulletType == BulletTypes.FromPlayer)
        {
            EnemyGetsHit(bullet);
        }
    }

    private void DeadDeadAnimationEnds()
    {
        Dispose();
    }

    private void EnemyGetsHit(Bullet bullet)
    {
        UpdateEnemyLife(bullet.Damage);
    }

    private void Update()
    {
        transform.Translate(Direction * Time.deltaTime * MovementSpeed);
        if (Time.realtimeSinceStartup - StartTime > LifeTime)
        {
            _signalBus.Fire(new EnemyDestroyedByTimeSignal(){ Enemy = this});

            Dispose();
        }
    }


    public void OnSpawned(EnemyInfo enemyInfo, IShot shotStrategy, IMemoryPool pool)
    {
        enemyInfo.ShotStrategy = shotStrategy;
        base.OnSpawned(enemyInfo, pool);
    }

    public void Shot()
    {
        if (!KilledByPlayer && gameObject.activeInHierarchy)
        {
            AudioManager.PlayMissileShotSoundEffect();
            ProjectileInfo projectileInfo = new ProjectileInfo()
            {
                MovementSpeed = _commonSettings.Enemy.Enemy3Settings.Missil.Speed,
                Life = _commonSettings.Enemy.Enemy3Settings.Missil.Life,
                LifeTime = _commonSettings.Enemy.Enemy3Settings.Missil.LifeTime,
                Damage = _commonSettings.Enemy.Enemy3Settings.Missil.Damage,
                Player = EnemyInfo.Player,
            };
            ShotStrategy.Shot(projectileInfo, _shotPoints);
        }
    }

    public class Factory : PlaceholderFactory<EnemyInfo, IShot, Enemy3>
    {
    }
}