// GENERATED AUTOMATICALLY FROM 'Assets/Code/GameplayInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @GameplayInput : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @GameplayInput()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""GameplayInput"",
    ""maps"": [
        {
            ""name"": ""Gameplay"",
            ""id"": ""58947089-b719-4b23-987e-fea0f6006aa4"",
            ""actions"": [
                {
                    ""name"": ""Interact"",
                    ""type"": ""Button"",
                    ""id"": ""319b9613-8c83-4704-9723-37ebb0dab2b0"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""AttackLeft"",
                    ""type"": ""Button"",
                    ""id"": ""03e2a2d6-f528-4915-9e5c-1dba06f85b1e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""AttackRight"",
                    ""type"": ""Button"",
                    ""id"": ""f11dc4a6-37cd-47e9-8157-ec40fa48aab1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ChangePlayerOne"",
                    ""type"": ""Button"",
                    ""id"": ""36a1fe5d-a532-4fe7-b672-85178646f87b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ChangePlayerTwo"",
                    ""type"": ""Button"",
                    ""id"": ""f28d0cd0-3f24-4bae-a293-acbeeb4f1805"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ChangePlayerThree"",
                    ""type"": ""Button"",
                    ""id"": ""08b06d62-31b8-4975-8793-daf8b142e15f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ChangeWeapon"",
                    ""type"": ""PassThrough"",
                    ""id"": ""4f3e0ff6-43cf-43fe-8b50-ae45174dbea1"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Movement"",
                    ""type"": ""Button"",
                    ""id"": ""dbb942ab-9e1e-4f9c-a9c7-de0905438f33"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""3a2dfa23-f1cb-48e3-8e06-32e17f0cc5a2"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e30b6a93-5f59-4a40-82f4-9dcdfb8d1b91"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AttackLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9dd55e95-0136-473b-8c7e-8c2c7f27aeab"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AttackRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""251508b1-0cc7-4c35-ad61-c0c2ff8a8f3b"",
                    ""path"": ""<Keyboard>/1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangePlayerOne"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7a5524e3-d37c-4c23-bcc9-462c20a4c490"",
                    ""path"": ""<Keyboard>/2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangePlayerTwo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bf964a96-ad64-415a-9d6e-bb99fc7bb134"",
                    ""path"": ""<Keyboard>/3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangePlayerThree"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""804d8f2e-008b-48b0-9928-957140cdbdac"",
                    ""path"": ""<Mouse>/scroll"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeWeapon"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""WASD"",
                    ""id"": ""7144b453-d7b4-4d8b-b054-67f5a65c8ed6"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""d526f90e-c041-4cda-8a23-2c357f402c2e"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""71d22817-b555-4b70-88a2-951d37d261b9"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""219d18e1-d77f-42b9-929f-531907ceb667"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""1ddff6c0-0f4f-4e16-9013-58227469036f"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Gameplay
        m_Gameplay = asset.FindActionMap("Gameplay", throwIfNotFound: true);
        m_Gameplay_Interact = m_Gameplay.FindAction("Interact", throwIfNotFound: true);
        m_Gameplay_AttackLeft = m_Gameplay.FindAction("AttackLeft", throwIfNotFound: true);
        m_Gameplay_AttackRight = m_Gameplay.FindAction("AttackRight", throwIfNotFound: true);
        m_Gameplay_ChangePlayerOne = m_Gameplay.FindAction("ChangePlayerOne", throwIfNotFound: true);
        m_Gameplay_ChangePlayerTwo = m_Gameplay.FindAction("ChangePlayerTwo", throwIfNotFound: true);
        m_Gameplay_ChangePlayerThree = m_Gameplay.FindAction("ChangePlayerThree", throwIfNotFound: true);
        m_Gameplay_ChangeWeapon = m_Gameplay.FindAction("ChangeWeapon", throwIfNotFound: true);
        m_Gameplay_Movement = m_Gameplay.FindAction("Movement", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Gameplay
    private readonly InputActionMap m_Gameplay;
    private IGameplayActions m_GameplayActionsCallbackInterface;
    private readonly InputAction m_Gameplay_Interact;
    private readonly InputAction m_Gameplay_AttackLeft;
    private readonly InputAction m_Gameplay_AttackRight;
    private readonly InputAction m_Gameplay_ChangePlayerOne;
    private readonly InputAction m_Gameplay_ChangePlayerTwo;
    private readonly InputAction m_Gameplay_ChangePlayerThree;
    private readonly InputAction m_Gameplay_ChangeWeapon;
    private readonly InputAction m_Gameplay_Movement;
    public struct GameplayActions
    {
        private @GameplayInput m_Wrapper;
        public GameplayActions(@GameplayInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @Interact => m_Wrapper.m_Gameplay_Interact;
        public InputAction @AttackLeft => m_Wrapper.m_Gameplay_AttackLeft;
        public InputAction @AttackRight => m_Wrapper.m_Gameplay_AttackRight;
        public InputAction @ChangePlayerOne => m_Wrapper.m_Gameplay_ChangePlayerOne;
        public InputAction @ChangePlayerTwo => m_Wrapper.m_Gameplay_ChangePlayerTwo;
        public InputAction @ChangePlayerThree => m_Wrapper.m_Gameplay_ChangePlayerThree;
        public InputAction @ChangeWeapon => m_Wrapper.m_Gameplay_ChangeWeapon;
        public InputAction @Movement => m_Wrapper.m_Gameplay_Movement;
        public InputActionMap Get() { return m_Wrapper.m_Gameplay; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GameplayActions set) { return set.Get(); }
        public void SetCallbacks(IGameplayActions instance)
        {
            if (m_Wrapper.m_GameplayActionsCallbackInterface != null)
            {
                @Interact.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnInteract;
                @Interact.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnInteract;
                @Interact.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnInteract;
                @AttackLeft.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnAttackLeft;
                @AttackLeft.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnAttackLeft;
                @AttackLeft.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnAttackLeft;
                @AttackRight.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnAttackRight;
                @AttackRight.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnAttackRight;
                @AttackRight.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnAttackRight;
                @ChangePlayerOne.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChangePlayerOne;
                @ChangePlayerOne.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChangePlayerOne;
                @ChangePlayerOne.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChangePlayerOne;
                @ChangePlayerTwo.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChangePlayerTwo;
                @ChangePlayerTwo.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChangePlayerTwo;
                @ChangePlayerTwo.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChangePlayerTwo;
                @ChangePlayerThree.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChangePlayerThree;
                @ChangePlayerThree.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChangePlayerThree;
                @ChangePlayerThree.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChangePlayerThree;
                @ChangeWeapon.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChangeWeapon;
                @ChangeWeapon.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChangeWeapon;
                @ChangeWeapon.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChangeWeapon;
                @Movement.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMovement;
                @Movement.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMovement;
                @Movement.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMovement;
            }
            m_Wrapper.m_GameplayActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Interact.started += instance.OnInteract;
                @Interact.performed += instance.OnInteract;
                @Interact.canceled += instance.OnInteract;
                @AttackLeft.started += instance.OnAttackLeft;
                @AttackLeft.performed += instance.OnAttackLeft;
                @AttackLeft.canceled += instance.OnAttackLeft;
                @AttackRight.started += instance.OnAttackRight;
                @AttackRight.performed += instance.OnAttackRight;
                @AttackRight.canceled += instance.OnAttackRight;
                @ChangePlayerOne.started += instance.OnChangePlayerOne;
                @ChangePlayerOne.performed += instance.OnChangePlayerOne;
                @ChangePlayerOne.canceled += instance.OnChangePlayerOne;
                @ChangePlayerTwo.started += instance.OnChangePlayerTwo;
                @ChangePlayerTwo.performed += instance.OnChangePlayerTwo;
                @ChangePlayerTwo.canceled += instance.OnChangePlayerTwo;
                @ChangePlayerThree.started += instance.OnChangePlayerThree;
                @ChangePlayerThree.performed += instance.OnChangePlayerThree;
                @ChangePlayerThree.canceled += instance.OnChangePlayerThree;
                @ChangeWeapon.started += instance.OnChangeWeapon;
                @ChangeWeapon.performed += instance.OnChangeWeapon;
                @ChangeWeapon.canceled += instance.OnChangeWeapon;
                @Movement.started += instance.OnMovement;
                @Movement.performed += instance.OnMovement;
                @Movement.canceled += instance.OnMovement;
            }
        }
    }
    public GameplayActions @Gameplay => new GameplayActions(this);
    public interface IGameplayActions
    {
        void OnInteract(InputAction.CallbackContext context);
        void OnAttackLeft(InputAction.CallbackContext context);
        void OnAttackRight(InputAction.CallbackContext context);
        void OnChangePlayerOne(InputAction.CallbackContext context);
        void OnChangePlayerTwo(InputAction.CallbackContext context);
        void OnChangePlayerThree(InputAction.CallbackContext context);
        void OnChangeWeapon(InputAction.CallbackContext context);
        void OnMovement(InputAction.CallbackContext context);
    }
}
