﻿using UnityEngine;

namespace Code.Signals
{
    public class SpawnNewPowerUpSignal
    {
        public Vector3 Position;
    }
}