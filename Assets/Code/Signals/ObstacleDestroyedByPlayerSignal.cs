﻿using Code.Obstacle;

namespace Code.Signals
{
    public class ObstacleDestroyedByPlayerSignal
    {
        public SpaceObstacle SpaceObstacle;
        public int Score;
    }
}