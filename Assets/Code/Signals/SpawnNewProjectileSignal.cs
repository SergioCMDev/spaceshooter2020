﻿using Code.Msile;
using UnityEngine;

namespace Code.Signals
{
    public class SpawnNewProjectileSignal
    {
        public Transform Position;
        public ProjectileType ProjectileType;
        public ProjectileInfo ProjectileInfo;
        public GameObject Creator;
    }
}