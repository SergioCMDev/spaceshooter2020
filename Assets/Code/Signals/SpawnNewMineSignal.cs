﻿using UnityEngine;

namespace Code.Signals
{
    public class SpawnNewMineSignal
    {
        public Vector3 Position;
        public Backgound Background;
    }
}