﻿namespace Code.Signals
{
    public class UpdateCanvasPowerUpValueSignal
    {
        public PowerUpType PowerUpType;
    }
}