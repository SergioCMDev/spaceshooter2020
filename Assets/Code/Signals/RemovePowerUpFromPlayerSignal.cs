﻿namespace Code.Signals
{
    public class RemovePowerUpFromPlayerSignal
    {
        public PowerUpInfo PowerUpInfo;
    }
}