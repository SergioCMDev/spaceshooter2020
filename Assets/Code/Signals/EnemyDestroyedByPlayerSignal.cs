﻿namespace Code.Signals
{
    public class EnemyDestroyedByPlayerSignal
    {
        public EnemyInfo EnemyInfo;
        public Enemy Enemy;
    }
}