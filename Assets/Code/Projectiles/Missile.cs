﻿using UnityEngine;
using Zenject;

namespace Code.Msile
{
    public struct ProjectileInfo
    {
        public float MovementSpeed;
        public float LifeTime;
        public float Life;
        public float Damage;
        public int ScoreForKill;
        public Vector3 Direction;
        public Quaternion Rotation;
        public Transform Player;
        public BulletTypes BulletTypes;
    }

    public class Missile : Projectile, IPoolable<ProjectileInfo, IMemoryPool>
    {
        private bool _hasExploded;

        private IMemoryPool _pool;
        private Transform _player;
        private Sprite _originalSprite;
        [SerializeField] private BoxCollider2D _boxCollider2D;
        [SerializeField] private bool _showRadiusExplosion = false;
        [SerializeField] private float _distanceToExplode;
        [SerializeField] private float _rotationSpeed = 2.0f;
        [SerializeField] private Transform _spriteTransform;
        private static readonly int Explode = Animator.StringToHash("Explode");
        [SerializeField] private Animator _animator;
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private ExpansiveWeaveCollider _expansiveWeaveCollider;
        [SerializeField] private SpriteController _spriteController;
        [Inject] private AudioManager _audioManager;

        private void Start()
        {
            _originalSprite = _spriteRenderer.sprite;
            _hasExploded = false;
            _showRadiusExplosion = false;
            _expansiveWeaveCollider.OnPlayerCollision += PlayerTriggeredCollider;
            _expansiveWeaveCollider.OnMissileCollision += PlayerTriggeredCollider;
            _spriteController.OnDeadAnimationEnds += DeadDeadAnimationEnds;
            CanHitPlayer = true;
            if (_player)
            {
                if ((_player.position - transform.position).x < 0.0f)
                {
                    _spriteTransform.Rotate(Vector3.forward, 180.0f);
                }
            }
        }

        private void DeadDeadAnimationEnds()
        {
            _spriteRenderer.sprite = _originalSprite;

            Dispose();
        }

        private void OnDestroy()
        {
            _expansiveWeaveCollider.OnPlayerCollision -= PlayerTriggeredCollider;
            _spriteController.OnDeadAnimationEnds -= DeadDeadAnimationEnds;
        }

        private void PlayerTriggeredCollider()
        {
            if (!_hasExploded)
            {
                _hasExploded = true;
                Destroy();
            }
        }


        public void Destroy()
        {
            _hasExploded = true;
            _audioManager.PlayMissileExplodingSoundEffect();

            _animator.SetTrigger(Explode);
        }

        private void Update()
        {
            if (_player)
            {
                Vector3 vectorToTarget = _player.position - _spriteTransform.position;
                float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
                Quaternion newRotation = Quaternion.AngleAxis(angle, Vector3.forward);
                _spriteTransform.rotation =
                    Quaternion.Slerp(_spriteTransform.rotation, newRotation, Time.deltaTime * _rotationSpeed);


                Direction = _player.transform.position - transform.position;
                transform.Translate(Direction * MovementSpeed * Time.deltaTime);
                if (!_hasExploded && Time.realtimeSinceStartup - StartTime > LifeTime)
                {
                    Destroy();
                }
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            Bullet bullet = other.GetComponent<Bullet>();
            Enemy enemy = other.GetComponent<Enemy>();
            if (enemy)
            {
                enemy.DestroyEnemy();
                Destroy();
                Debug.Log("CHOQUE CON ENEMY");
            }

            if (bullet && !_hasExploded)
            {
                Destroy();
            }
        }

        public void OnSpawned(ProjectileInfo projectileInfo, IMemoryPool pool)
        {
            _pool = pool;
            _hasExploded = false;
            _player = projectileInfo.Player;
            _originalSprite = _spriteRenderer.sprite;
            base.OnSpawned(projectileInfo, pool);
        }


        private void OnDrawGizmos()
        {
            if (_showRadiusExplosion)
                Gizmos.DrawCube(transform.position, _boxCollider2D.size);


            Debug.DrawLine(transform.position, (transform.position + Direction * _distanceToExplode), Color.blue);
        }


        public class Factory : PlaceholderFactory<ProjectileInfo, Missile>
        {
        }
    }
}