﻿using Code.Msile;
using Code.Obstacle;
using UnityEngine;
using Zenject;

namespace Code
{
    public enum BulletTypes
    {
        FromEnemy,
        FromPlayer
    }

    public class Bullet : Projectile, IPoolable<ProjectileInfo, BulletTypes, IMemoryPool>
    {
        [SerializeField] private Transform _bulletSprite;
        [SerializeField] private Sprite _bulletSpriteRed;
        [SerializeField] private Sprite _bulletSpriteOrange;
        [SerializeField] private Sprite _bulletSpriteBlue;
        [SerializeField] private Sprite _bulletSpriteGreen;
        [Inject] private GameSettingsInstaller _commonSettings;

        private BulletTypes _type;

        public BulletTypes BulletType
        {
            get => _type;
        }

        public void Dispose()
        {
            Creator = null;
            base.Dispose();
        }

        void Update()
        {
            transform.Translate(Direction * Time.deltaTime * MovementSpeed);
            if (Time.realtimeSinceStartup - StartTime > LifeTime)
            {
                Dispose();
            }
        }


        private void OnTriggerEnter2D(Collider2D other)
        {
            Enemy enemy = other.GetComponent<Enemy>();
            Player player = other.GetComponent<Player>();
            SpaceObstacle obstacle = other.GetComponent<SpaceObstacle>();
            if (BulletFromEnemyHitsPlayer(player) || BulletFromPlayerHitsEnemy(enemy) || obstacle)
            {
                Dispose();
            }
        }

        private bool BulletFromPlayerHitsEnemy(Enemy enemy)
        {
            return _type == BulletTypes.FromPlayer && enemy;
        }

        private bool BulletFromEnemyHitsPlayer(Player player)
        {
            return _type == BulletTypes.FromEnemy && player;
        }

        public void OnSpawned(ProjectileInfo projectileInfo, BulletTypes type, IMemoryPool pool)
        {
            base.OnSpawned(projectileInfo, pool);
            _type = type;
            CanHitPlayer = true;

            SetBulletSettings(projectileInfo, type);
        }

        private void SetBulletSettings(ProjectileInfo projectileInfo, BulletTypes type)
        {
            if (type == BulletTypes.FromEnemy)
            {
                transform.rotation = projectileInfo.Rotation;

                _bulletSprite.GetComponent<SpriteRenderer>().sprite = _bulletSpriteGreen;
            }
            else
            {
                transform.rotation = projectileInfo.Rotation;

                if (projectileInfo.Damage >= _commonSettings.Player.PlayerBulletsMinDamageToChangeToRedSprite)
                {
                    _bulletSprite.GetComponent<SpriteRenderer>().sprite = _bulletSpriteRed;
                }
                else if (projectileInfo.Damage >= _commonSettings.Player.PlayerBulletsMinDamageToChangeToOrangeSprite)
                {
                    _bulletSprite.GetComponent<SpriteRenderer>().sprite = _bulletSpriteOrange;
                }
                else if (projectileInfo.Damage < _commonSettings.Player.PlayerBulletsMinDamageToChangeToRedSprite)
                {
                    _bulletSprite.GetComponent<SpriteRenderer>().sprite = _bulletSpriteBlue;
                }
            }
        }

        public class Factory : PlaceholderFactory<ProjectileInfo, BulletTypes, Bullet>
        {
        }
    }
}