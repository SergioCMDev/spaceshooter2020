﻿using Code.Msile;
using UnityEngine;
using Zenject;

namespace Code
{
    public enum ProjectileType
    {
        Bullet,
        Missile
    }
    public abstract class Projectile : MonoBehaviour
    {
        protected Vector3 Direction;
        protected float LifeTime;
        protected float MovementSpeed;
        protected float StartTime;
        public float Damage;
        private IMemoryPool _pool;
        public GameObject Creator;
        public bool CanHitPlayer { get; set; }

        public void OnSpawned(ProjectileInfo projectileInfo, IMemoryPool pool)
        {
            _pool = pool;
            Damage = projectileInfo.Damage;
            MovementSpeed = projectileInfo.MovementSpeed;
            LifeTime = projectileInfo.LifeTime;
            Direction = projectileInfo.Direction;
            StartTime = Time.realtimeSinceStartup;
        }
        
        public void Dispose()
        {
            gameObject.SetActive(false);
            _pool?.Despawn(this);
        }
        
        public void OnDespawned()
        {
            _pool = null;
        }
    }
}