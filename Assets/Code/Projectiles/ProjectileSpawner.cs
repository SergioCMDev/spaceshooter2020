﻿using System;
using Code.Msile;
using Code.Signals;
using UnityEngine;
using Zenject;

namespace Code
{
    public class ProjectileSpawner : MonoBehaviour
    {
        [Inject] private readonly SignalBus _signalBus;
        [Inject] readonly protected Bullet.Factory _bulletFactory;
        [Inject] readonly protected Missile.Factory _missileFactory;
        [Inject] private IProjectileModel _projectileModel;

        void Start()
        {
            _signalBus.Subscribe<SpawnNewProjectileSignal>(SpawnProjectile);
        }

        private void SpawnProjectile(SpawnNewProjectileSignal signal)
        {
            Projectile projectile;
            switch (signal.ProjectileType)
            {
                case ProjectileType.Bullet:
                    projectile = _bulletFactory.Create(signal.ProjectileInfo,
                        signal.ProjectileInfo.BulletTypes);
                    break;
                case ProjectileType.Missile:
                    projectile = _missileFactory.Create(signal.ProjectileInfo);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            projectile.Creator = signal.Creator;
            projectile.transform.position = signal.Position.position;
            _projectileModel.Projectiles.Add(projectile);
        }


        public void Reset()
        {
            foreach (var projectile in _projectileModel.Projectiles)
            {
                projectile.Dispose();
            }

            _projectileModel.Projectiles.Clear();
        }
    }
}