﻿using Code.Signals;
using UnityEngine;
using Zenject;

public struct PowerUpInfo
{
    public PowerUpType PowerUpType;
    public float Value;
    public float LifeTimeInPlayer;
    public Sprite Sprite;
    public float LifeTimeInSpace;
}

public enum PowerUpType
{
    MovementSpeedPlayer,
    BulletsSpeed,
    BulletDamage,
    Shield,
    MultipleShot
}

public class PowerUp : MonoBehaviour, IPoolable<PowerUpInfo, IMemoryPool>
{
    [SerializeField] private BoxCollider2D _boxCollider2D;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [Inject] private readonly SignalBus _signalBus;
    private IMemoryPool _pool;

    private float _startTime;
    private bool _caughtByPlayer;

    private PowerUpInfo _powerPowerUpInfo;


    public PowerUpInfo PowerUpInfo
    {
        get => _powerPowerUpInfo;
    }

    public void Dispose()
    {
        _pool?.Despawn(this);
    }

    public void OnDespawned()
    {
        _pool = null;
    }

    void Update()
    {
        if (!_caughtByPlayer && Time.realtimeSinceStartup - _startTime > _powerPowerUpInfo.LifeTimeInSpace)
        {
            Dispose();
        }

        if (_caughtByPlayer && Time.realtimeSinceStartup - _startTime > _powerPowerUpInfo.LifeTimeInPlayer)
        {
            _signalBus.Fire(new RemovePowerUpFromPlayerSignal()
            {
                PowerUpInfo = _powerPowerUpInfo
            });
            Dispose();
        }
    }

    public void CaughtByPlayer()
    {
        _startTime = Time.realtimeSinceStartup;
        _caughtByPlayer = true;
        _boxCollider2D.enabled = false;
        _spriteRenderer.enabled = false;
    }


    public void OnSpawned(PowerUpInfo powerUpInfo, IMemoryPool pool)
    {
        _pool = pool;

        _powerPowerUpInfo = powerUpInfo;
        _startTime = Time.realtimeSinceStartup;

        _boxCollider2D.enabled = true;
        _spriteRenderer.enabled = true;
        _spriteRenderer.sprite = powerUpInfo.Sprite;

        _caughtByPlayer = false;
    }


    public class Factory : PlaceholderFactory<PowerUpInfo, PowerUp>
    {
    }
}