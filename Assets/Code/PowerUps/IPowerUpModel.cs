﻿using System.Collections.Generic;

namespace Code.PowerUps
{
    public interface IPowerUpModel
    {
        List<PowerUp> PowerUps { get; set; }
    }

}