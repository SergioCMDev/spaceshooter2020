﻿using System.Collections.Generic;
using Zenject;

namespace Code.PowerUps
{
    public class PowerUpModel : IPowerUpModel, IInitializable
    {
        public List<PowerUp> PowerUps { get; set; }

        public void Initialize()
        {
            PowerUps = new List<PowerUp>();
        }
    }
}