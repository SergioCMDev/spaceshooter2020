﻿using Code;
using Code.PowerUps;
using Code.Signals;
using UnityEngine;
using Zenject;

public class PowerUpSpawner : MonoBehaviour
{
    [Inject] readonly private PowerUp.Factory _powerUpFactory;

    [Inject] private readonly SignalBus _signalBus;
    [Inject] private IPowerUpModel _powerUpModel;
    [SerializeField] private bool _speedPlayer;
    [SerializeField] private bool _bulletDamage;
    [SerializeField] private bool _shields;
    [SerializeField] private bool _multipleShot;
    [SerializeField] private bool _bulletSpeed;
    [SerializeField] private Sprite _speedPlayerSprite;
    [SerializeField] private Sprite _bulletDamageSprite;
    [SerializeField] private Sprite _shieldsSprite;
    [SerializeField] private Sprite _multipleShotSprite;
    [SerializeField] private Sprite _speedBulletsSprite;
    [Inject] private GameSettingsInstaller _commonSettings;

    void Start()
    {
        _signalBus.Subscribe<SpawnNewPowerUpSignal>(GeneratePowerUp);
        //TODO GET SPRITES FROM ADDRESSABLES
    }

    public void GeneratePowerUp(SpawnNewPowerUpSignal signal)
    {
        int powerUpRandom = Random.Range(0, 100);
        PowerUp powerUp;

        if (powerUpRandom >= _commonSettings.PowerUps.MinProbabiltyToSpawnMovementSpeed &&
            powerUpRandom <= _commonSettings.PowerUps.MaxProbabiltyToSpawnMovementSpeed && _speedPlayer)
        {
            PowerUpInfo powerUpInfo = new PowerUpInfo()
            {
                LifeTimeInPlayer = _commonSettings.PowerUps.PowerUpMovementSpeed.LifeTimeAtPlayer,
                LifeTimeInSpace = _commonSettings.PowerUps.PowerUpMovementSpeed.LifeTimeAtSpace,
                Value = _commonSettings.PowerUps.PowerUpMovementSpeed.ImproveValue,
            };
            powerUpInfo.Sprite = _speedPlayerSprite;
            powerUpInfo.PowerUpType = PowerUpType.MovementSpeedPlayer;
            powerUp = _powerUpFactory.Create(powerUpInfo);
            SavePowerUp(signal, powerUp);
        }
        else if (powerUpRandom > _commonSettings.PowerUps.MinProbabiltyToSpawnBulletDamage &&
                 powerUpRandom <= _commonSettings.PowerUps.MaxProbabiltyToSpawnBulletDamage && _bulletDamage)
        {
            PowerUpInfo powerUpInfo = new PowerUpInfo()
            {
                LifeTimeInPlayer = _commonSettings.PowerUps.PowerUpBulletDamage.LifeTimeAtPlayer,
                LifeTimeInSpace = _commonSettings.PowerUps.PowerUpBulletDamage.LifeTimeAtSpace,
                Value = _commonSettings.PowerUps.PowerUpBulletDamage.ImproveValue,
            };
            powerUpInfo.Sprite = _bulletDamageSprite;

            powerUpInfo.PowerUpType = PowerUpType.BulletDamage;
            powerUp = _powerUpFactory.Create(powerUpInfo);
            SavePowerUp(signal, powerUp);
        }
        else if (powerUpRandom > _commonSettings.PowerUps.MinProbabiltyToSpawnShield &&
                 powerUpRandom <= _commonSettings.PowerUps.MaxProbabiltyToSpawnShield && _shields)
        {
            PowerUpInfo powerUpInfo = new PowerUpInfo()
            {
                LifeTimeInPlayer = _commonSettings.PowerUps.PowerUpShield.LifeTimeAtPlayer,
                LifeTimeInSpace = _commonSettings.PowerUps.PowerUpShield.LifeTimeAtSpace,
                Value = _commonSettings.PowerUps.PowerUpShield.ImproveValue,
            };
            powerUpInfo.Sprite = _shieldsSprite;

            powerUpInfo.PowerUpType = PowerUpType.Shield;
            powerUp = _powerUpFactory.Create(powerUpInfo);
            SavePowerUp(signal, powerUp);
        }
        else if (powerUpRandom > _commonSettings.PowerUps.MinProbabiltyToSpawnMultipleShot &&
                 powerUpRandom <= _commonSettings.PowerUps.MaxProbabiltyToSpawnMultipleShot && _multipleShot)
        {
            PowerUpInfo powerUpInfo = new PowerUpInfo()
            {
                LifeTimeInPlayer = _commonSettings.PowerUps.PowerUpMultipleShot.LifeTimeAtPlayer,
                LifeTimeInSpace = _commonSettings.PowerUps.PowerUpMultipleShot.LifeTimeAtSpace,
                Value = _commonSettings.PowerUps.PowerUpMultipleShot.ImproveValue,
            };
            powerUpInfo.Sprite = _multipleShotSprite;

            powerUpInfo.PowerUpType = PowerUpType.MultipleShot;
            powerUp = _powerUpFactory.Create(powerUpInfo);
            SavePowerUp(signal, powerUp);
        }
        else if (powerUpRandom > _commonSettings.PowerUps.MinProbabiltyToSpawnBulletSpeed &&
                 powerUpRandom <= _commonSettings.PowerUps.MaxProbabiltyToSpawnBulletSpeed && _bulletSpeed)
        {
            PowerUpInfo powerUpInfo = new PowerUpInfo()
            {
                LifeTimeInPlayer = _commonSettings.PowerUps.PowerUpBulletSpeed.LifeTimeAtPlayer,
                LifeTimeInSpace = _commonSettings.PowerUps.PowerUpBulletSpeed.LifeTimeAtSpace,
                Value = _commonSettings.PowerUps.PowerUpBulletSpeed.ImproveValue,
            };
            powerUpInfo.Sprite = _speedBulletsSprite;

            powerUpInfo.PowerUpType = PowerUpType.BulletsSpeed;
            powerUp = _powerUpFactory.Create(powerUpInfo);
            SavePowerUp(signal, powerUp);
        }
    }

    private void SavePowerUp(SpawnNewPowerUpSignal signal, PowerUp powerUp)
    {
        _powerUpModel.PowerUps.Add(powerUp);
        powerUp.transform.position = signal.Position;
    }

    public void Reset()
    {
        foreach (var powerUp in _powerUpModel.PowerUps)
        {
            powerUp.Dispose();
        }

        _powerUpModel.PowerUps.Clear();
    }
}