﻿using UnityEngine;

namespace Code.Commands
{
    public abstract class AttackCommand : Command
    {
        public abstract void Execute(Transform transform, Command command);

        protected abstract void Attack();
    }
}