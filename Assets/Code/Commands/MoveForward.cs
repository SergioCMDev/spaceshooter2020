﻿using UnityEngine;

namespace Code.Commands
{
    public class MoveForward : MoveCommand
    {
        public override void Execute(Transform transform, Command command, float powerUpSpeed)
        {
            base.powerUpSpeed = powerUpSpeed;

            Move(transform);
        }

        public MoveForward(float baseSpeed) : base(baseSpeed)
        {
            base.BaseSpeed = baseSpeed;
        }


        protected override void Move(Transform playerTransform)
        {
            Debug.Log(base.BaseSpeed + " " + powerUpSpeed);

            playerTransform.Translate(Vector3.up * Time.deltaTime * (base.BaseSpeed + powerUpSpeed));
        }
    }
}