﻿using UnityEngine;

namespace Code.Commands
{
    public abstract class MoveCommand : Command
    {
        public abstract void Execute(Transform transform, Command command, float powerUpsPlayerModel);

        public MoveCommand(float baseSpeed)
        {
            this.BaseSpeed = baseSpeed;
        }

        protected float BaseSpeed = 5;
        protected float powerUpSpeed = 0;
        protected abstract void Move(Transform playerTransform);
    }
}