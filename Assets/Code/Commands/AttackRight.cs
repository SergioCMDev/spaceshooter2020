﻿using System.ComponentModel;
using UnityEngine;
using Zenject;

namespace Code.Commands
{
    public class AttackRight : AttackCommand
    {
        private Transform _playerTransform;
        private Player _player;

        protected override void Attack()
        {
            Debug.Log("R");
            // GameObject.Instantiate(_bullet, _playerTransform, false);
            // _player.CreateBullet();
        }


        public override void Execute(Transform transform, Command command)
        {
            _playerTransform = transform;
            _player = _playerTransform.GetComponent<Player>();
            Attack();
        }
    }
}