﻿using UnityEngine;

namespace Code.Commands
{
    public class MoveRight : MoveCommand
    {
        public override void Execute(Transform transform, Command command, float powerUpSpeed)
        {
            base.powerUpSpeed = powerUpSpeed;

            Move(transform);
        }

        protected override void Move(Transform playerTransform)
        {
            Debug.Log(base.BaseSpeed +" "+powerUpSpeed);

            playerTransform.Translate(Vector3.right * Time.deltaTime  * (base.BaseSpeed + powerUpSpeed));
        }

        public MoveRight(float baseSpeed) : base(baseSpeed)
        {
        }
    }
}