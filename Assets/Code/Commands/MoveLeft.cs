﻿using UnityEngine;

namespace Code.Commands
{
    public class MoveLeft : MoveCommand
    {
        private float _powerUpSpeed = 0;
        public override void Execute(Transform transform, Command command, float powerUpSpeed)
        {
            base.powerUpSpeed = powerUpSpeed;

            Move(transform);
        }

        protected override void Move(Transform playerTransform)
        {
            Debug.Log(base.BaseSpeed +" "+powerUpSpeed);

            playerTransform.Translate(Vector3.left * Time.deltaTime * (base.BaseSpeed + powerUpSpeed));
        }

        public MoveLeft(float baseSpeed) : base(baseSpeed)
        {
        }
    }
}