﻿using UnityEngine;

namespace Code.Commands
{
    public class MoveBack : MoveCommand
    {
        public override void Execute(Transform transform, Command command, float powerUpSpeed)
        {
            base.powerUpSpeed = powerUpSpeed;

            Move(transform);
        }

   

        protected override void Move(Transform playerTransform)
        {
            Debug.Log(base.BaseSpeed +" "+powerUpSpeed);

            playerTransform.Translate(-Vector3.up * Time.deltaTime  * (base.BaseSpeed + powerUpSpeed));
        }

        public MoveBack(float baseSpeed) : base(baseSpeed)
        {
        }
        
    }
}