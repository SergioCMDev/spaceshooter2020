﻿using System.Collections.Generic;
using Code.Msile;
using Code.Signals;
using UnityEngine;
using Zenject;

namespace Code
{
    public class ShotOneBullet : MonoBehaviour, IShot, IInitializable, IPoolable<IMemoryPool>
    {
        [Inject] private SignalBus _signalBus;
        private IMemoryPool _pool;

        public void Shot(ProjectileInfo projectileInfo, List<Transform> shotPoints)
        {
            _signalBus.Fire(new SpawnNewProjectileSignal()
            {
                Position = shotPoints[0],
                ProjectileInfo = projectileInfo,
                ProjectileType = ProjectileType.Bullet
            });
        }

        public void Initialize()
        {
            // throw new System.NotImplementedException();
        }

        public class Factory : PlaceholderFactory<ShotOneBullet>
        {
        }

        public void OnDespawned()
        {
            _pool = null;
        }

        public void OnSpawned(IMemoryPool p2)
        {
            _pool = p2;
        }
    }
}