﻿using System.Collections.Generic;
using Code.Msile;
using Code.Signals;
using UnityEngine;
using Zenject;

namespace Code
{
    public class ShotSeveralBullets : MonoBehaviour, IShot, IInitializable,IPoolable<IMemoryPool>
    {
        [Inject] private SignalBus _signalBus;
        private IMemoryPool _pool;
        public void Shot(ProjectileInfo projectileInfo, List<Transform> shotPoints)
        {
            foreach (var shotPoint in shotPoints)
            {
                _signalBus.Fire(new SpawnNewProjectileSignal()
                {
                    Position = shotPoint,
                    ProjectileInfo = projectileInfo,
                    ProjectileType = ProjectileType.Bullet
                });
            }
        }

        public void Initialize()
        {
            // throw new System.NotImplementedException();
        }
        
        public class Factory : PlaceholderFactory<ShotSeveralBullets>
        {
        }

        public void OnDespawned()
        {
            _pool = null;
        }

        public void OnSpawned(IMemoryPool p2)
        {
            _pool = p2;
        }

    }
}