﻿using System.Collections.Generic;
using Code.Msile;
using UnityEngine;

namespace Code
{
    public interface IShot
    {
        void Shot(ProjectileInfo projectileInfo, List<Transform> shotPoints);
    }
}