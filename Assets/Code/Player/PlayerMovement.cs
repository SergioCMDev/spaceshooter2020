﻿using System;
using Code;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using Zenject;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private ReadInputPlayer _inputPlayer;
    [Inject] private IPlayerInfoModel _playerModel;
    [SerializeField] private RectangleToSpawn _maxPositionsValues;
    private float _rotation;
    [SerializeField] private bool _EnableRotationWithMouse;
    [SerializeField] private float _rotationSpeed = 10;
    public float Rotation => _rotation;

    private void Update()
    {
        if (_playerModel.EnableRotationWithMouse || _EnableRotationWithMouse)
        {
            Vector2Control mousePositionFirst = Mouse.current.position;
            var mousePositionOnWorld = Camera.main.ScreenToWorldPoint(mousePositionFirst.ReadValue());

            Vector3 vectorToTarget = transform.position - mousePositionOnWorld;
            float rotation = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
            Quaternion newRotation = Quaternion.AngleAxis(rotation, Vector3.forward);
            transform.rotation =
                Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * _rotationSpeed);
        }
    }

    void FixedUpdate()
    {
        if (_inputPlayer.MovementAxis.magnitude > 0.01f && _playerModel.Life > 0)
        {
            transform.Translate(_inputPlayer.MovementAxis * _playerModel.Speed *
                                Time.fixedDeltaTime);

            Vector3 positionPlayer = transform.position;


            positionPlayer.x = Mathf.Clamp(positionPlayer.x, _maxPositionsValues.MinPosition.x,
                _maxPositionsValues.MaxPosition.x);
            positionPlayer.y = Mathf.Clamp(positionPlayer.y, _maxPositionsValues.MinPosition.y,
                _maxPositionsValues.MaxPosition.y);

            transform.position = positionPlayer;
        }
    }
}