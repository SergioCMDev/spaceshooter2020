﻿using System;
using System.Collections.Generic;
using Code.Msile;
using Code.Obstacle;
using Code.Signals;
using UnityEngine;
using Zenject;

namespace Code
{
    public class Player : MonoBehaviour
    {
        [SerializeField] private ReadInputPlayer _inputPlayer;
        [SerializeField] private Animator _animator;
        [SerializeField] private bool _invencible;
        [SerializeField] private List<Transform> _shotPoints;
        [SerializeField] private PlayerMovement _playerMovement;
        [SerializeField] private Shield _shield;
        [Inject] private AudioManager _audioManager;
        [Inject] private IPlayerInfoModel _playerInfoModel;
        [Inject] private ShotOneBullet.Factory _factoryBullets;
        [Inject] private ShotSeveralBullets.Factory _factorySeveralBullets;
        [Inject] private GameSettingsInstaller _commonSettings;

        [Inject] private SignalBus _signalBus;
        private IShot _shotBulletsStrategy;
        private IShot _shotSeveralBulletsStrategy;
        private static readonly int Explode = Animator.StringToHash("Explode");


        private void Awake()
        {
            _inputPlayer.OnPlayerAttackRight += CreateBullet;
            _signalBus.Subscribe<RemovePowerUpFromPlayerSignal>(RemovePowerUp);
            _shotBulletsStrategy = _factoryBullets.Create();
            _shotSeveralBulletsStrategy = _factorySeveralBullets.Create();
        }

        private void RemovePowerUp(RemovePowerUpFromPlayerSignal powerUp)
        {
            switch (powerUp.PowerUpInfo.PowerUpType)
            {
                case PowerUpType.MovementSpeedPlayer:
                    _playerInfoModel.Speed -= powerUp.PowerUpInfo.Value;
                    break;
                case PowerUpType.BulletsSpeed:
                    _playerInfoModel.BulletsSpeed -= powerUp.PowerUpInfo.Value;
                    break;
                case PowerUpType.BulletDamage:
                    _playerInfoModel.BulletDamage -= powerUp.PowerUpInfo.Value;
                    break;
                case PowerUpType.Shield:
                    _playerInfoModel.ShieldsTime -= powerUp.PowerUpInfo.Value;
                    _shield.gameObject.SetActive(false);
                    break;
                case PowerUpType.MultipleShot:
                    _playerInfoModel.MultipleShotsTime -= powerUp.PowerUpInfo.Value;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            _signalBus.Fire(new UpdateCanvasPowerUpValueSignal()
                {PowerUpType = powerUp.PowerUpInfo.PowerUpType});
        }

        private void CreateBullet(bool obj)
        {
            Shot();
        }

        public void Shot()
        {
            if (_playerInfoModel.Life <= 0)
            {
                return;
            }

            _audioManager.PlayPlayerLaserSoundEffect();
            float bulletLifeTime = 5;
            ProjectileInfo projectileInfo = new ProjectileInfo()
            {
                MovementSpeed = _playerInfoModel.BulletsSpeed,
                Damage = _playerInfoModel.BulletDamage,
                Direction = Vector3.up,
                LifeTime = bulletLifeTime,
                BulletTypes = BulletTypes.FromPlayer,
                Rotation = transform.rotation
            };
            if (_playerInfoModel.MultipleShotsTime > 0)
            {
                _shotSeveralBulletsStrategy.Shot(projectileInfo, _shotPoints);
            }
            else
            {
                _shotBulletsStrategy.Shot(projectileInfo, _shotPoints);
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            Backgound background = other.GetComponent<Backgound>();

            if (background)
            {
                return;
            }

            PowerUp powerUp = other.GetComponent<PowerUp>();
            if (powerUp)
            {
                PlayerGetsPowerUp(powerUp);
            }

            if (!PlayerHaveShield() && !_invencible)
            {
                PlayerGetsHit(other);
            }
        }

        private bool PlayerHaveShield()
        {
            return _playerInfoModel.ShieldsTime > 0;
        }

        private void PlayerGetsHit(Collider2D other)
        {
            Projectile projectile = other.GetComponent<Projectile>();
            SpaceObstacle obstacle = other.GetComponent<SpaceObstacle>();
            Enemy enemy = other.GetComponent<Enemy>();
            ExpansiveWeaveCollider expansiveWeave = other.GetComponent<ExpansiveWeaveCollider>();

            if (expansiveWeave)
            {
                if (expansiveWeave.GetComponentInParent<Mine>())
                {
                    obstacle = expansiveWeave.GetComponentInParent<Mine>();
                }
                else
                {
                    projectile = other.GetComponentInParent<Projectile>();
                }
            }

            float damage = 0;

            if (projectile && projectile.CanHitPlayer)
            {
                projectile.CanHitPlayer = false;
                damage = projectile.Damage;
            }
            else if (obstacle)
            {
                if (obstacle.Type == ObstacleType.Meteor)
                {
                    damage = obstacle.Damage;
                    _audioManager.PlayMeteorHitSoundEffect();
                }
                else if (obstacle.Type == ObstacleType.Mine)
                {
                    Mine mine = (Mine) obstacle;
                    if (mine.HasExploded && mine.CanHitPlayer)
                    {
                        damage = obstacle.Damage;
                        mine.CanHitPlayer = false;
                    }
                }
            }
            else if (enemy && !enemy.Destroyed)
            {
                _audioManager.PlayPlayerHitsEnemySoundEffect();
                enemy.DestroyEnemy();
                _playerInfoModel.Life -= _commonSettings.Enemy.DamageByHit;
            }

            _playerInfoModel.Life -= (int) damage;
            if (_playerInfoModel.Life <= 0)
            {
                _audioManager.PlayPlayerExplodingSoundEffect();
                _animator.SetTrigger(Explode);
            }

            _signalBus.Fire(new PlayerGetsHitSignal());
        }

        //Called By Explode Animation
        public void ExplodeAnimationEnds()
        {
            _signalBus.Fire(new PlayerIsDeadSignal());
        }

        private void PlayerGetsPowerUp(PowerUp powerUp)
        {
            _audioManager.PlayPickingPowerUpSoundEffect();
            switch (powerUp.PowerUpInfo.PowerUpType)
            {
                case PowerUpType.MovementSpeedPlayer:
                    _playerInfoModel.Speed += powerUp.PowerUpInfo.Value;
                    break;
                case PowerUpType.BulletsSpeed:
                    _playerInfoModel.BulletsSpeed += powerUp.PowerUpInfo.Value;
                    break;
                case PowerUpType.BulletDamage:
                    _playerInfoModel.BulletDamage += powerUp.PowerUpInfo.Value;
                    break;
                case PowerUpType.Shield:
                    if (_playerInfoModel.ShieldsTime > 0)
                    {
                        break;
                    }
                    _playerInfoModel.ShieldsTime += powerUp.PowerUpInfo.Value;
                    _shield.gameObject.SetActive(true);
                    break;
                case PowerUpType.MultipleShot:
                    _playerInfoModel.MultipleShotsTime += powerUp.PowerUpInfo.Value;
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            _signalBus.Fire(new UpdateCanvasPowerUpValueSignal()
            {
                PowerUpType = powerUp.PowerUpInfo.PowerUpType
            });

            powerUp.CaughtByPlayer();
        }
    }
}