﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

public class ReadInputPlayer : MonoBehaviour
{
    [Inject] private SignalBus _signalBus;

    [SerializeField] private float _sensitivity;

    private GameplayInput _playerInputActions;
    private Vector2 _movementInput;
    private float valueOfChangePlayer;

    public event Action<bool> OnPlayerAttackRight = delegate { };

    public Vector2 MovementAxis
    {
        get { return _movementInput; }
    }

    private void OnEnable()
    {
        _playerInputActions?.Enable();
    }

    private void OnDisable()
    {
        _playerInputActions?.Disable();
    }

    private void Awake()
    {
        _playerInputActions = new GameplayInput();


        _playerInputActions.Gameplay.Movement.performed += PlayerMove;
        _playerInputActions.Gameplay.Movement.canceled += PlayerStopMoving;

        _playerInputActions.Gameplay.AttackRight.performed += ctx => AttackRight();
        // _playerInputActions.Gameplay.AttackLeft.performed += ctx => AttackRight();


        _playerInputActions.Gameplay.ChangeWeapon.performed += ChangeWeapon;
    }

    private void PlayerStopMoving(InputAction.CallbackContext obj)
    {
        this._movementInput = Vector2.zero;
    }

    private void PlayerMove(InputAction.CallbackContext obj)
    {
        if (obj.ReadValue<Vector2>().magnitude > 0.01f)
        {
            this._movementInput = obj.ReadValue<Vector2>();
        }
    }

    //TODO In case we want to add several weapons to the player
    private void ChangeWeapon(InputAction.CallbackContext ctx)
    {
        if (ctx.ReadValue<Vector2>().y > 0)
        {
            // _signalBus.Fire(new ChangeToUpperCharacterSignal());
        }
        else if (ctx.ReadValue<Vector2>().y < 0)
        {
            // _signalBus.Fire(new ChangeToBottomCharacterSignal());
        }
    }


    private void AttackRight()
    {
        OnPlayerAttackRight.Invoke(true);
    }
}