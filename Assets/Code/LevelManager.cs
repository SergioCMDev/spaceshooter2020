﻿using System;
using System.Collections;
using System.Collections.Generic;
using Code;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private CanvasPresenter _canvasPresenter;
    [SerializeField] private ObstaclesSpawner _obstaclesSpawner;
    [SerializeField] private PowerUpSpawner _powerUpSpawner;
    [SerializeField] private EnemySpawner _enemySpawner;
    [SerializeField] private ProjectileSpawner _projectileSpawner;
    [Inject] private IProjectileModel _projectileModel;

    private void Start()
    {
        _canvasPresenter.OnRestartLevel += RestartLevel;
        _canvasPresenter.OnExitLevel += ExitLevel;
    }

    private void OnDestroy()
    {
        _canvasPresenter.OnRestartLevel -= RestartLevel;
        _canvasPresenter.OnExitLevel -= ExitLevel;
    }

    private void ExitLevel()
    {
        Application.Quit();

    }

    private void RestartLevel()
    {
        _obstaclesSpawner.Reset();
        _powerUpSpawner.Reset();
        _enemySpawner.Reset();
        _projectileSpawner.Reset();
        foreach (var projectile in  _projectileModel.Projectiles)
        {
            projectile.Dispose();
        }
        _projectileModel.Projectiles.Clear();

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}