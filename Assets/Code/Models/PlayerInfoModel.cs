﻿using UnityEngine;
using Zenject;

namespace Code
{
    public class PlayerInfoModel : IPlayerInfoModel, IInitializable
    {
        [Inject] private GameSettingsInstaller _commonSettings;

        public int BaseLife { get; set; }
        public float BaseSpeed { get; set; }
        public float BaseShields { get; set; }

        public float BaseBulletDamage { get; set; }

        public float BaseSpeedBullets { get; set; }
        public float BaseMultipleShotsTime { get; set; }
        public float Speed { get; set; }
        public float ShieldsTime { get; set; }
        public int Life { get; set; }
        public float BulletsSpeed { get; set; }
        public float MultipleShotsTime { get; set; }
        public bool EnableRotationWithMouse { get; set; }

        public float BulletDamage { get; set; }
        public void Init()
        {
            Initialize();
        }


        public void Initialize()
        {
            BaseSpeed = _commonSettings.Player.BaseMovementSpeed;
            BaseLife = _commonSettings.Player.BaseLife;
            BaseSpeedBullets = _commonSettings.Player.BaseBulletsSpeed;
            BaseBulletDamage = _commonSettings.Player.BaseBulletsDamage;
            BaseShields = _commonSettings.Player.BaseShieldsTime;
            BaseMultipleShotsTime = _commonSettings.Player.BaseMultipleShotsTime;
            
            Speed = BaseSpeed;
            ShieldsTime = BaseShields;
            Life = BaseLife;
            BulletsSpeed = BaseSpeedBullets;
            BulletDamage = BaseBulletDamage;
            MultipleShotsTime = BaseMultipleShotsTime;
        }
    }
}