﻿namespace Code
{
    public interface ILevelInfoModel
    {
        int Score { get; set; }
        int EnemiesDefeated { get; set; }
        int ObstaclesDestroyed { get; set; }
        void Init();
    }
}