﻿using System.Collections.Generic;
using Zenject;

namespace Code
{
    public class ProjectileModel : IProjectileModel, IInitializable
    {
        public List<Projectile> Projectiles { get; set; }

        public void Initialize()
        {
            Projectiles = new List<Projectile>();
        }
    }
}