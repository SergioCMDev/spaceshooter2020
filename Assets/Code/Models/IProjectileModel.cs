﻿using System.Collections.Generic;

namespace Code
{
    public interface IProjectileModel
    {
        List<Projectile> Projectiles { get; set; }

    }
}