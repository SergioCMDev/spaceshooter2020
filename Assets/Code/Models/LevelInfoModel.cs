﻿using Zenject;

namespace Code
{
    public class LevelInfoModel : ILevelInfoModel, IInitializable
    {
        public int Score { get; set; }
        public int EnemiesDefeated { get; set; }
        public int ObstaclesDestroyed { get; set; }
        public void Init()
        {
            Initialize();
        }

        public void Initialize()
        {
            Score = 0;
            EnemiesDefeated = 0;
            ObstaclesDestroyed = 0;
        }
    }
}