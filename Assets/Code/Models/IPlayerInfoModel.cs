﻿namespace Code
{
    public interface IPlayerInfoModel
    {
        int BaseLife { get; }
        float BaseSpeed { get; }
        float BaseShields { get; }
        float BaseSpeedBullets { get; }
        float BaseBulletDamage { get; }

        float Speed { get; set; }
        float ShieldsTime { get; set; }
        int Life { get; set; }
        float BulletsSpeed { get; set; }
        float BulletDamage { get; set; }
        float MultipleShotsTime { get; set; }

        bool EnableRotationWithMouse { get; set; }

        void Init();
    }
}