﻿using System;
using System.Collections.Generic;
using Code;
using Code.Obstacle;
using Code.Signals;
using Code.Utils;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

[Serializable]
public class RectangleToSpawn
{
    public Vector2 MinPosition;
    public Vector2 MaxPosition;
}

public class ObstaclesSpawner : MonoBehaviour
{
    [Inject] private CustomObstacleFactory.ObstacleFactory _obstacleFactory;
    [Inject] private readonly SignalBus _signalBus;
    [Inject] private ILevelInfoModel _levelInfoModel;
    [Inject] private IObstaclesModel _obstaclesModel;

    [SerializeField] private List<RectangleToSpawn> positionsToSpawnMeteors;
    [SerializeField] private RectangleToSpawn _positionsToSpawnMines;
    [SerializeField] private bool _meteors;
    [SerializeField] private bool _mines;
    [SerializeField] private Player _player;
    private float _startTime;
    [Inject] private GameSettingsInstaller _commonSettings;

    private int _maxObstaclesToSpawn;
    private int _obstaclesSpawned = 0;

    void Start()
    {
        InvokeRepeating("SpawnObstacle", _commonSettings.SpaceObstacles.TimeToStartSpawningObstacles, _commonSettings.SpaceObstacles.TimeToSpawnObstaclesOnceStarts);
        _signalBus.Subscribe<ObstacleDestroyedByPlayerSignal>(ObstacleDestroyed);
        _maxObstaclesToSpawn = _commonSettings.SpaceObstacles.MaxObstaclesToSpawn;
    }


    private void SpawnObstacle()
    {
        int obstacleRandom = Random.Range(0, 100);
        SpaceObstacle obstacle;
     

        int randomPositionToSpawn = Random.Range(0, 100);
        //We only have two positions to spawn, in case we had more we should change this
        if (randomPositionToSpawn < 50)
        {
            randomPositionToSpawn = 0;
        }
        else
        {
            randomPositionToSpawn = 1;
        }

        Vector2 newPosition = Vector2.zero;

        //TODO ADD MORE OBSTACLES
        if (obstacleRandom >= _commonSettings.SpaceObstacles.MinProbabilityToSpawnMeteors &&
            obstacleRandom <= _commonSettings.SpaceObstacles.MaxProbabilityToSpawnMeteors && _meteors)
        {
            ObstacleInfo meterorInfo = new ObstacleInfo()
            {
                Speed = _commonSettings.SpaceObstacles.MeteorSpeed,
                Life = _commonSettings.SpaceObstacles.MeteorLife,
                LifeTime = _commonSettings.SpaceObstacles.MeteorLifeTime,
                Damage = _commonSettings.SpaceObstacles.MeteorDamage,
                ObstacleType = ObstacleType.Meteor,

                Player = _player.transform,
            };
            newPosition = Utils.GetRandomPositionToSpawn(positionsToSpawnMeteors[randomPositionToSpawn]);

            obstacle = _obstacleFactory.Create(meterorInfo, ObstacleType.Meteor);

            SetInfoOfNewSpaceObstacle(obstacle, newPosition);
        }
        else if (obstacleRandom > _commonSettings.SpaceObstacles.MinProbabilityToSpawnMine &&
                 obstacleRandom < _commonSettings.SpaceObstacles.MaxProbabilityToSpawnMine && _mines)
        {
            MineInfo mineInfo = new MineInfo()
            {
                Speed = _commonSettings.SpaceObstacles.MineSpeed,
                Life = _commonSettings.SpaceObstacles.MineLife,
                LifeTime = _commonSettings.SpaceObstacles.MineLifeTime,
                Damage = _commonSettings.SpaceObstacles.MineDamage,
                ObstacleType = ObstacleType.Mine,
                Player = _player.transform,
            };
            newPosition = Utils.GetRandomPositionToSpawn(_positionsToSpawnMines);

            obstacle = _obstacleFactory.Create(mineInfo, ObstacleType.Mine);

            SetInfoOfNewSpaceObstacle(obstacle, newPosition);
        }


        _obstaclesSpawned++;
    }

    private void SetInfoOfNewSpaceObstacle(SpaceObstacle obstacle, Vector2 newPosition)
    {
        _obstaclesModel.Obstacles.Add(obstacle.GetComponent<SpaceObstacle>());
        obstacle.transform.position = newPosition;
    }

    private void OnDrawGizmos()
    {
        for (int i = 0; i < positionsToSpawnMeteors.Count; i++)
        {
            Vector2 vector1 = new Vector2(positionsToSpawnMeteors[i].MinPosition.x,
                positionsToSpawnMeteors[i].MinPosition.y);
            Vector2 vector2 = new Vector2(positionsToSpawnMeteors[i].MinPosition.x,
                positionsToSpawnMeteors[i].MaxPosition.y);
            Vector2 vector3 = new Vector2(positionsToSpawnMeteors[i].MaxPosition.x,
                positionsToSpawnMeteors[i].MaxPosition.y);
            Vector2 vector4 = new Vector2(positionsToSpawnMeteors[i].MaxPosition.x,
                positionsToSpawnMeteors[i].MinPosition.y);

            Debug.DrawLine(vector1, vector2, Color.red);
            Debug.DrawLine(vector2, vector3, Color.red);
            Debug.DrawLine(vector3, vector4, Color.red);
            Debug.DrawLine(vector4, vector1, Color.red);
        }

        Vector2 vector1Mines = new Vector2(_positionsToSpawnMines.MinPosition.x, _positionsToSpawnMines.MinPosition.y);
        Vector2 vector2Mines = new Vector2(_positionsToSpawnMines.MinPosition.x, _positionsToSpawnMines.MaxPosition.y);
        Vector2 vector3Mines = new Vector2(_positionsToSpawnMines.MaxPosition.x, _positionsToSpawnMines.MaxPosition.y);
        Vector2 vector4mines = new Vector2(_positionsToSpawnMines.MaxPosition.x, _positionsToSpawnMines.MinPosition.y);

        Debug.DrawLine(vector1Mines, vector2Mines, Color.yellow);
        Debug.DrawLine(vector2Mines, vector3Mines, Color.yellow);
        Debug.DrawLine(vector3Mines, vector4mines, Color.yellow);
        Debug.DrawLine(vector4mines, vector1Mines, Color.yellow);
    }

    private void ObstacleDestroyed(ObstacleDestroyedByPlayerSignal signal)
    {
        _levelInfoModel.Score += signal.Score;
        _levelInfoModel.ObstaclesDestroyed++;
        _signalBus.Fire(new UpdateGameInfoPointsSignal());

        _obstaclesSpawned--;
    }

    public void Reset()
    {
        foreach (var obstacle in _obstaclesModel.Obstacles)
        {
            obstacle.Dispose();
        }

        _obstaclesModel.Obstacles.Clear();
    }
}