﻿using System.Collections.Generic;
using Zenject;

namespace Code.Obstacle
{
    public class ObstaclesModel : IObstaclesModel, IInitializable
    {
        public List<SpaceObstacle> Obstacles { get; set; }

        public void Initialize()
        {
            Obstacles = new List<SpaceObstacle>();
        }
    }
}