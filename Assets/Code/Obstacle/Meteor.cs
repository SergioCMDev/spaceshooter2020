﻿using Code.Signals;
using UnityEngine;
using Zenject;

namespace Code.Obstacle
{
    public class Meteor : SpaceObstacle, IPoolable<ObstacleInfo, IMemoryPool>
    {
        [Inject] private SignalBus _signalBus;
        [Inject] private GameSettingsInstaller _commonSettings;

        private void Start()
        {
            Direction = Player.transform.position - transform.position;
        }

        private void Update()
        {
            transform.Translate(Direction * MovementSpeed * Time.deltaTime);
            if (Time.realtimeSinceStartup - StartTime > LifeTime)
            {
                Dispose();
            }
        }

        public void OnSpawned(ObstacleInfo obstacleInfo, IMemoryPool pool)
        {
            base.OnSpawned(obstacleInfo, pool);
            Type = ObstacleType.Meteor;
            Player = obstacleInfo.Player;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            Bullet bullet = other.GetComponent<Bullet>();
            Player player = other.GetComponent<Player>();
            if (bullet || player)
            {
                Dispose();
                if (bullet && bullet.BulletType == BulletTypes.FromPlayer)
                {
                    _signalBus.Fire(new ObstacleDestroyedByPlayerSignal()
                        {SpaceObstacle = this, Score = _commonSettings.SpaceObstacles.ScoreForDestroyMeteors});
                }
            }
        }

        public class Factory : PlaceholderFactory<ObstacleInfo, Meteor>
        {
        }
    }
}