﻿using Code.Obstacle;
using Zenject;

namespace Code
{
    public class CustomObstacleFactory : IFactory<ObstacleInfo, ObstacleType, SpaceObstacle>
    {
        [Inject] private Meteor.Factory _meteorFactory1;
        [Inject] private Mine.Factory _mineFactory;


        public SpaceObstacle Create(ObstacleInfo obstacleInfo, ObstacleType type)
        {
            switch (type)
            {
                case ObstacleType.Meteor:
                    return _meteorFactory1.Create(obstacleInfo);
                case ObstacleType.Mine:
                    MineInfo mineInfo = (MineInfo) obstacleInfo;
                    return _mineFactory.Create(mineInfo);

                default:
                    return _meteorFactory1.Create(obstacleInfo);
            }
        }

        public class ObstacleFactory : PlaceholderFactory<ObstacleInfo, ObstacleType, SpaceObstacle>
        {
        }
    }
}