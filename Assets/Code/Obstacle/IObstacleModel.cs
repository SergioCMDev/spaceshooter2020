﻿using System.Collections.Generic;

namespace Code.Obstacle
{
    public interface IObstaclesModel
    {
        List<SpaceObstacle> Obstacles { get; set; }
    }
}