﻿
using UnityEngine;

namespace Code.Obstacle
{
    public class ObstacleInfo
    {
        public float Speed;
        public float Life;
        public float Damage;
        public float LifeTime;
        public ObstacleType ObstacleType;
        public Transform Player;

    }
}