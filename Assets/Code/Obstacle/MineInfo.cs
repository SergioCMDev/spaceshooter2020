﻿namespace Code.Obstacle
{
    public class MineInfo : ObstacleInfo
    {
        public float RadiusToExplode;
        public float RadiusWhileExploding;
    }
}