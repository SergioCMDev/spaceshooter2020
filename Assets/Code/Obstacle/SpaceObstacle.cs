﻿using System;
using UnityEngine;
using Zenject;

namespace Code.Obstacle
{
    public enum ObstacleType
    {
        Meteor,
        Mine,
        Type3
    }

    public class SpaceObstacle : MonoBehaviour, IDisposable
    {
        public float Damage;
        protected float MovementSpeed;
        protected float Life;
        public bool KilledByPlayer;
        protected float LifeTime;
        protected Vector3 Direction;
        protected Transform Player;
        private ObstacleType _type;
        protected float StartTime;

        IMemoryPool _pool;

        public ObstacleType Type
        {
            get => _type;
            set => _type = value;
        }
        
        public void OnDespawned()
        {
            _pool = null;
            gameObject.SetActive(false);
        }

        public void OnSpawned(ObstacleInfo obstacleInfo, IMemoryPool pool)
        {
            _pool = pool;
            MovementSpeed = obstacleInfo.Speed;
            Life = obstacleInfo.Life;
            LifeTime = obstacleInfo.LifeTime;
            KilledByPlayer = false;
            Damage = obstacleInfo.Damage;

            StartTime = Time.realtimeSinceStartup;
        }

        public void Dispose()
        {
            _pool?.Despawn(this);
        }
    }
}