﻿using Code.Obstacle;
using Code.Signals;
using UnityEngine;
using Zenject;

namespace Code
{
    public class Mine : SpaceObstacle, IPoolable<MineInfo, IMemoryPool>
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private ExpansiveWeaveCollider _expansiveWeaveCollider;
        [Inject] private AudioManager _audioManager;
        [Inject] private SignalBus _signalBus;
        [Inject] private GameSettingsInstaller _commonSettings;

        private bool _hasExploded;
        private bool _canHitPlayer;
        private static readonly int Explode = Animator.StringToHash("Explode");
        private Sprite _sprite;

        public bool HasExploded
        {
            get => _hasExploded;
            set => _hasExploded = value;
        }

        public bool CanHitPlayer
        {
            get => _canHitPlayer;
            set => _canHitPlayer = value;
        }

        private void Start()
        {
            _sprite = _spriteRenderer.sprite;
            _expansiveWeaveCollider.OnPlayerCollision += PlayerTriggeredCollider;
        }

        private void OnDestroy()
        {
            _expansiveWeaveCollider.OnPlayerCollision -= PlayerTriggeredCollider;
        }

        private void PlayerTriggeredCollider()
        {
            if (!_hasExploded)
            {
                _hasExploded = true;
                MineExplode();
            }
        }

        public void OnSpawned(MineInfo info, IMemoryPool pool)
        {
            _hasExploded = false;
            Player = info.Player;
            _canHitPlayer = true;
            Type = ObstacleType.Mine;
            base.OnSpawned(info, pool);
        }

        public void MineExplode()
        {
            _hasExploded = true;
            if (KilledByPlayer)
            {
                _signalBus.Fire(new ObstacleDestroyedByPlayerSignal()
                    {SpaceObstacle = this, Score = _commonSettings.SpaceObstacles.ScoreForDestroyMines});
            }

            _animator.SetTrigger(Explode);
            _audioManager.PlayMineExplodingSoundEffect();
        }

        //Called by Animator
        public void ExplodeAnimationEnds()
        {
            _spriteRenderer.sprite = _sprite;

            Dispose();
        }

        private void Update()
        {
            transform.Translate(Vector3.down * MovementSpeed * Time.deltaTime);
            if (!_hasExploded && Time.realtimeSinceStartup - StartTime > LifeTime)
            {
                MineExplode();
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            Bullet bullet = other.GetComponent<Bullet>();
            if (bullet && !_hasExploded)
            {
                Life -= bullet.Damage;
                if (Life <= 0)
                {
                    KilledByPlayer = true;
                    MineExplode();
                }
            }
        }


        public class Factory : PlaceholderFactory<MineInfo, Mine>
        {
        }
    }
}