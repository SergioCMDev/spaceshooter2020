﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundParallax : MonoBehaviour
{
    [SerializeField] private List<Backgound> _backgrounds;
    [SerializeField] private LayerMask _terrainLayerMask;
    [SerializeField] private float _movementSpeed;
    private float _sizeTerrainBlock;
    private int _backgroundsPassed;
    private Backgound _blockToMove;

    void Start()
    {
        _sizeTerrainBlock = _backgrounds[2].transform.position.y -
                            _backgrounds[1].transform.position.y;
        _sizeTerrainBlock = Mathf.Abs(_sizeTerrainBlock);
        for (int i = 0; i < _backgrounds.Count; i++)
        {
            _backgrounds[i].OnMoveBackground += MoveBackground;
            // _backgrounds[i].OnGenerateMines += GenerateElementsForNewsTerrains;
        }
    }

    // private void GenerateElementsForNewsTerrains(Backgound obj)
    // {
    //     obj.GenerateMines();
    // }

    private void OnDestroy()
    {
        for (int i = 0; i < _backgrounds.Count; i++)
        {
            _backgrounds[i].OnMoveBackground -= MoveBackground;
            // _backgrounds[i].OnGenerateElementsForNextTerrain += GenerateElementsForNewsTerrains;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.down * _movementSpeed * Time.deltaTime);
    }


    private void MoveBackground(Backgound background)
    {
        _backgroundsPassed++;

        if (_backgroundsPassed == _backgrounds.Count-1)
        {
            _backgroundsPassed--;

            Vector3 positionOfOtheTerrain =
                GetPositionOfTheOtherBlockWithMaxY();
            Vector3 newPosition = positionOfOtheTerrain +
                                  Vector3.up * _sizeTerrainBlock;
            // Debug.Log("New Position of terrain moved "+newPosition.z);

            _blockToMove.MoveToNextPosition(newPosition);
            // _blockToMove.GenerateMines();
        }

        _blockToMove = background;
    }


    private Vector3 GetPositionOfTheOtherBlockWithMaxY()
    {
        int idOfMaxY = 0;
        float minY = Int32.MinValue;

        for (int i = 0; i < _backgrounds.Count; i++)
        {
            if (_backgrounds[i].transform.position.y > minY)
            {
                minY = _backgrounds[i].transform.position.y;
                idOfMaxY = i;
            }
        }

        return _backgrounds[idOfMaxY].transform.position;
    }

    private Backgound GetTerrainWithMinY()
    {
        int idOfMinY = 0;
        float maximumY = Int32.MaxValue;

        for (int i = 0; i < _backgrounds.Count; i++)
        {
            if (_backgrounds[i].transform.position.y < maximumY)
            {
                maximumY = _backgrounds[i].transform.position.y;
                idOfMinY = i;
            }
        }

        return _backgrounds[idOfMinY];
    }
}