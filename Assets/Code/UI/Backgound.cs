﻿using System;
using System.Collections.Generic;
using Code;
using Code.Signals;
using UnityEngine;
using Zenject;

public class Backgound : MonoBehaviour
{
    [SerializeField] private BoxCollider2D _triggerChangeBackground;
    public event Action<Backgound> OnMoveBackground = delegate { };
    public event Action<Backgound> OnGenerateMines = delegate { };

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Player>())
        {
            OnMoveBackground.Invoke(this);
            _triggerChangeBackground.enabled = false;
        }
    }

    public void MoveToNextPosition(Vector3 newPosition)
    {
        transform.SetPositionAndRotation(newPosition, transform.rotation);
        _triggerChangeBackground.enabled = true;
    }
}