﻿using UnityEngine;
using UnityEngine.UI;

namespace Energy
{
    public class HPBarView : MonoBehaviour
    {
        [SerializeField] private Slider _slider;
        [SerializeField] private Gradient _gradient;
        [SerializeField] private Image _fill;

        public void SetMaxValue(float energy)
        {
            _slider.maxValue = energy;
            _gradient.Evaluate(1f);
            SetValue(energy);
        }

        public void SetValue(float energy)
        {
            _slider.value = energy;
            _fill.color = _gradient.Evaluate(_slider.normalizedValue);
        }
    }
}