﻿using System;
using Code;
using Code.Signals;
using Code.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class CanvasPresenter : MonoBehaviour
{

    
    [Inject] private SignalBus _signalBus;
    [Inject] private IPlayerInfoModel _playerInfoModel;
    [Inject] private ILevelInfoModel _levelInfoModel;
    [Inject] private EndGameMenuView.Factory _factoryEndMenuView;
    [Inject] private AudioManager _audioManager;
    [SerializeField] private GameInfoView _gameInfoView;
    [SerializeField] private PlayerInfoView _playerInfoView;
    [SerializeField] private Button _restartButton;
    [SerializeField] private Button _exitButton;
    private EndGameMenuView _endMenu;
    public event Action OnRestartLevel = delegate { };
    public event Action OnExitLevel = delegate { };

    void Start()
    {
        _restartButton.onClick.AddListener(RestartGame);
        _exitButton.onClick.AddListener(ExitGame);
        _audioManager.SetStatusBackgroundMusic(true);
        Utils.PauseGame(false);
        _playerInfoModel.Init();
        _levelInfoModel.Init();
        _levelInfoModel.Init();
        _signalBus.Subscribe<UpdateGameInfoPointsSignal>(UpdateScorePoints);
        _signalBus.Subscribe<PlayerGetsHitSignal>(UpdatePlayerLife);
        _signalBus.Subscribe<UpdateCanvasPowerUpValueSignal>(UpdateCanvasPowerUpValue);
        _signalBus.Subscribe<PlayerIsDeadSignal>(ShowEndGameMenu);

        _gameInfoView.SetScore(_levelInfoModel.Score);
        _gameInfoView.SetEnemiesDefeated(_levelInfoModel.EnemiesDefeated);
        _playerInfoView.SetMaxHPBar(_playerInfoModel.BaseLife);
        _playerInfoView.SetShieldValue(_playerInfoModel.BaseShields);
        _playerInfoView.SetSpeedValue(_playerInfoModel.BaseSpeed);
        _playerInfoView.SetBulletSpeed(_playerInfoModel.BaseSpeedBullets);
        _playerInfoView.SetBulletDamage(_playerInfoModel.BaseBulletDamage);
    }

    private void UpdateCanvasPowerUpValue(UpdateCanvasPowerUpValueSignal obj)
    {
        switch (obj.PowerUpType)
        {
            case PowerUpType.MovementSpeedPlayer:
                _playerInfoView.SetSpeedValue(_playerInfoModel.Speed);
                break;
            case PowerUpType.BulletsSpeed:
                _playerInfoView.SetBulletSpeed(_playerInfoModel.BulletsSpeed);
                break;
            case PowerUpType.BulletDamage:
                _playerInfoView.SetBulletDamage(_playerInfoModel.BulletDamage);
                break;
            case PowerUpType.Shield:
                _playerInfoView.SetShieldValue(_playerInfoModel.ShieldsTime);
                break;
            case PowerUpType.MultipleShot:
                // _playerInfoView.SetMultipleShotsValue(_playerInfoModel.MultipleShotsTime);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }


    private void UpdatePlayerLife(PlayerGetsHitSignal obj)
    {
        _playerInfoView.PlayerGetsHit(_playerInfoModel.Life);
    }

    public void ShowEndGameMenu(PlayerIsDeadSignal signal)
    {
        _audioManager.SetStatusBackgroundMusic(false);

        _endMenu = _factoryEndMenuView.Create(_levelInfoModel.Score,
            Utils.RoundValue(Time.timeSinceLevelLoad, 1), _levelInfoModel.EnemiesDefeated);

        _endMenu.transform.SetParent(transform, false);
        _endMenu.OnPressRestart += RestartGame;
        _endMenu.OnPressExit += ExitGame;
        Utils.PauseGame(true);
        _gameInfoView.StopCounter();
    }

    private void OnDestroy()
    {
        _restartButton.onClick.RemoveListener(RestartGame);

        _signalBus?.TryUnsubscribe<UpdateGameInfoPointsSignal>(UpdateScorePoints);
        _signalBus?.TryUnsubscribe<PlayerGetsHitSignal>(UpdatePlayerLife);
        _signalBus?.TryUnsubscribe<UpdateCanvasPowerUpValueSignal>(UpdateCanvasPowerUpValue);
        _signalBus?.TryUnsubscribe<PlayerIsDeadSignal>(ShowEndGameMenu);

        if (_endMenu)
        {
            _endMenu.OnPressRestart -= RestartGame;
            _endMenu.OnPressExit -= ExitGame;
        }
    }

    private void ExitGame()
    {
        _audioManager.PlayUiClickSoundEffect();
        OnExitLevel.Invoke();
    }

    private void RestartGame()
    {
        _audioManager.PlayUiClickSoundEffect();
        OnRestartLevel.Invoke();
    }

    private void UpdateScorePoints(UpdateGameInfoPointsSignal obj)
    {
        _gameInfoView.SetScore(_levelInfoModel.Score);
        _gameInfoView.SetEnemiesDefeated(_levelInfoModel.EnemiesDefeated);
    }
}