﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class EndGameMenuView : MonoBehaviour, IPoolable<int, float, int, IMemoryPool>, IDisposable
{
    [SerializeField] private TextMeshProUGUI _scoreValue;
    [SerializeField] private TextMeshProUGUI _timeValue;
    [SerializeField] private TextMeshProUGUI _enemiesDefeated;
    [SerializeField] private Button _restartButton;
    [SerializeField] private Button _exitButton;
    private IMemoryPool _pool;
    [Inject] private AudioManager _audioManager;


    public class Factory : PlaceholderFactory<int, float, int, EndGameMenuView>
    {
    }

    public event Action OnPressRestart = delegate { };
    public event Action OnPressExit = delegate { };

    private void Start()
    {
        _restartButton.onClick.AddListener(Restart);
        _exitButton.onClick.AddListener(ExitGame);
    }

    private void ExitGame()
    {
        _audioManager.PlayUiClickSoundEffect();

        Dispose();
        OnPressExit.Invoke();
    }

    private void Restart()
    {
        _audioManager.PlayUiClickSoundEffect();
        Dispose();

        OnPressRestart.Invoke();
    }


    public void OnDespawned()
    {
        _pool = null;
    }

    public void OnSpawned(int score, float time, int enemiesDefeated, IMemoryPool p4)
    {
        _scoreValue.SetText(score.ToString());
        _timeValue.SetText(time.ToString());
        _enemiesDefeated.SetText(enemiesDefeated.ToString());
    }

    public void Dispose()
    {
        _pool?.Despawn(this);
    }
}