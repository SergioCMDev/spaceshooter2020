﻿using Code;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

public class CanvasInitMenuPresenter : MonoBehaviour
{
    [SerializeField] private Button _startButton;
    [SerializeField] private Button _exitButton;
    [SerializeField] private Toggle _toggle;
    [Inject] private AudioManager _audioManager;
    [Inject] private IPlayerInfoModel _playerInfoModel;

    void Start()
    {
        _startButton.onClick.AddListener(StartGame);
        _exitButton.onClick.AddListener(ExitGame);
        _toggle.isOn = false;
    }

    private void ExitGame()
    {
        _audioManager.PlayUiClickSoundEffect();

        Application.Quit();
    }

    private void OnDestroy()
    {
        _startButton.onClick.RemoveListener(StartGame);
        _exitButton.onClick.RemoveListener(ExitGame);

    }

    private void StartGame()
    {
        _audioManager.PlayUiClickSoundEffect();
        _playerInfoModel.EnableRotationWithMouse = _toggle.isOn;
        SceneManager.LoadScene(1);
    }
}
