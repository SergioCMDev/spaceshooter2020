﻿using Code;
using Code.Utils;
using TMPro;
using UnityEngine;

public class GameInfoView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _scoreValue;
    [SerializeField] private TextMeshProUGUI _timeValue;
    [SerializeField] private TextMeshProUGUI _enemiesDefeatedValue;
    private bool _count = true;

    private float _timeSinceStart;


    // Update is called once per frame
    void Update()
    {
        if (_count)
        {
            _timeSinceStart = Time.timeSinceLevelLoad;
            _timeValue.SetText(Utils.RoundValue(_timeSinceStart, 1).ToString());
        }
    }

    public void SetScore(int score)
    {
        _scoreValue.SetText(score.ToString());
    }

    public void SetEnemiesDefeated(int score)
    {
        _enemiesDefeatedValue.SetText(score.ToString());
    }

    public void StopCounter()
    {
        _count = false;
    }
}