﻿using System.Collections;
using System.Collections.Generic;
using Code.Utils;
using Energy;
using TMPro;
using UnityEngine;

public class PlayerInfoView : MonoBehaviour
{
    [SerializeField] private HPBarView _hpBarView;

    [SerializeField] private TextMeshProUGUI _speedValue;
    [SerializeField] private TextMeshProUGUI _shieldValue;
    [SerializeField] private TextMeshProUGUI _bulletDamageValue;
    [SerializeField] private TextMeshProUGUI _BulletSpeedValue;


    public void SetSpeedValue(float speed)
    {
        _speedValue.SetText(speed.ToString());
    }

    public void SetShieldValue(float shieldValue)
    {
        StartCoroutine(SubstractTime(shieldValue));
        // _shieldValue.SetText(shieldValue.ToString());
    }

    IEnumerator SubstractTime(float time)
    {
        float passedTime = 0;
        float remainnigTime = 0;
        while (passedTime < time)
        {
            passedTime += Time.deltaTime;
            remainnigTime = time - passedTime;
            _shieldValue.SetText(Utils.RoundValue(remainnigTime, 1).ToString());
            yield return null;
        }

        _shieldValue.SetText(0.ToString());
    }

    public void PlayerGetsHit(int remainingLife)
    {
        _hpBarView.SetValue(remainingLife);
    }

    public void SetMaxHPBar(int playerLife)
    {
        _hpBarView.SetMaxValue(playerLife);
    }

    public void SetBulletDamage(float objBulletDamageImproveValue)
    {
        _bulletDamageValue.SetText(objBulletDamageImproveValue.ToString());
    }

    public void SetBulletSpeed(float objBulletSpeeddImproveValue)
    {
        _BulletSpeedValue.SetText(objBulletSpeeddImproveValue.ToString());
    }
}