﻿

using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.ResourceLocations;

namespace Code.Utils
{
    public static class CreateAddressableLoader
    {
        public static async Task ByLoadedAddress<T>(IList<IResourceLocation> loadedLocation, List<T> cretedObjects)
            where T : Object
        {
            foreach (var location in loadedLocation)
            {
                var obj = await Addressables.InstantiateAsync(location).Task as T;
                cretedObjects.Add(obj);
            }
        }
    }
}