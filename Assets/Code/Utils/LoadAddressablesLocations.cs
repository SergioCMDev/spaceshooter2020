﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

namespace Code.Utils
{
    public static class LoadAddressablesLocations
    {
        public static async Task LoadLocations<T>(string label,
            IList<IResourceLocation> loadedLocations, Action<AsyncOperationHandle<IList<IResourceLocation>>> callback)
        {
            var loadResources = Addressables.LoadResourceLocationsAsync(label, typeof(T));
            if (callback != null)
            {
                loadResources.Completed += callback;
            }

            var locations = await loadResources.Task;

            foreach (var resourceLocation in locations)
                loadedLocations.Add(resourceLocation);
        }

        public static async Task GetAssetsOnLocations<T>(IList<IResourceLocation> loadedLocations,
            IList<T> listOfAssets, Action<AsyncOperationHandle<IList<T>>> callback)
        {
            var r = Addressables.LoadAssetsAsync<T>(loadedLocations, null);
            if (callback != null)
            {
                r.Completed += callback;
            }

            var listOfAssetGenerated = await r.Task;

            foreach (var resourceLocation in listOfAssetGenerated)
                listOfAssets.Add(resourceLocation);
        }
    }
}