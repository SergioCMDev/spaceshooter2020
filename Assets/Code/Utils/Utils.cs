﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Code.Utils
{
    public static class Utils
    {
        public static Vector2 GetRandomPositionToSpawn(RectangleToSpawn positionsToSpawn)
        {
            float randomPositionX = Random.Range(positionsToSpawn.MinPosition.x, positionsToSpawn.MaxPosition.x);
            float randomPositionY = Random.Range(positionsToSpawn.MinPosition.y, positionsToSpawn.MaxPosition.y);

            return new Vector2(randomPositionX, randomPositionY);
        }

        public static float RoundValue(float value, int decimals)
        {
            return (float) Decimal.Round((decimal) value, decimals);
        }

        public static void PauseGame(bool value)
        {
            Time.timeScale = value ? 0 : 1;
        }

        public static bool IsGamePaused()
        {
            return Time.timeScale == 0;
        }
    }
}