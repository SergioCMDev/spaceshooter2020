﻿using System;
using Code;
using Code.Msile;
using UnityEngine;

public class ExpansiveWeaveCollider : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        Player player = other.GetComponent<Player>();
        Missile missile = other.GetComponent<Missile>();

        if (player)
        {
            OnPlayerCollision.Invoke();
        }

        // if (missile)
        // {
        //     OnMissileCollision.Invoke();
        // }
    }
 
    public event Action OnPlayerCollision = delegate { };
    public event Action OnMissileCollision = delegate { };
}