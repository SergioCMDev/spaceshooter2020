using Code.Signals;
using Zenject;

namespace Code
{
    public class PlayerInstaller : MonoInstaller<PlayerInstaller>
    {
        public override void InstallBindings()
        {
            BindModels();
            BindSignals();
        }

        private void BindSignals()
        {
            Container.DeclareSignal<UpdateGameInfoPointsSignal>();
            Container.DeclareSignal<PlayerGetsHitSignal>();
            Container.DeclareSignal<ObstacleDestroyedByPlayerSignal>();
            Container.DeclareSignal<RemovePowerUpFromPlayerSignal>();
            Container.DeclareSignal<UpdateCanvasPowerUpValueSignal>();
            Container.DeclareSignal<EnemyDestroyedByPlayerSignal>();
            Container.DeclareSignal<PlayerIsDeadSignal>();
            
            
        }

        private void BindModels()
        {
            Container.BindInterfacesAndSelfTo<PlayerInfoModel>().AsSingle();
        }
    }
}