﻿using Code.Obstacle;
using Code.PowerUps;
using UnityEngine;
using Zenject;

namespace Code
{
    public class GameInstaller : MonoInstaller<GameInstaller>
    {
        [SerializeField] private GameSettingsInstaller _settings;

        public override void InstallBindings()
        {
            SignalBusInstaller.Install(Container);
            Container.Bind<GameSettingsInstaller>().FromInstance(_settings);
            BindModels();
        }

        private void BindModels()
        {
            Container.BindInterfacesAndSelfTo<LevelInfoModel>().AsSingle();
            Container.BindInterfacesAndSelfTo<ObstaclesModel>().AsSingle();
            Container.BindInterfacesAndSelfTo<PowerUpModel>().AsSingle();
            Container.BindInterfacesAndSelfTo<ProjectileModel>().AsSingle();
            Container.BindInterfacesAndSelfTo<EnemyModel>().AsSingle();
        }
    }
}