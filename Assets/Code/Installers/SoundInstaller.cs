﻿using UnityEngine;
using Zenject;

public class SoundInstaller : MonoInstaller<SoundInstaller>
{
    [SerializeField] private AudioManager _audioManagerPrefab;

    public override void InstallBindings()
    {
        Container.Bind<AudioManager>().FromComponentInNewPrefab(_audioManagerPrefab).AsSingle();
    }
}