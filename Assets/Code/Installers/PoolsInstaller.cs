﻿using Code.Msile;
using Code.Obstacle;
using Code.Signals;
using UnityEngine;
using Zenject;

namespace Code
{
    public class PoolsInstaller : MonoInstaller<PoolsInstaller>
    {
        [SerializeField] private Bullet _bulletPrefab;
        [SerializeField] private GameObject _enemy1Prefab;
        [SerializeField] private GameObject _enemy2Prefab;
        [SerializeField] private GameObject _enemy3Prefab;
        [SerializeField] private PowerUp _powerUpPrefab;
        [SerializeField] private GameObject _endGamePrefab;
        [SerializeField] private GameObject _obstacle1Prefab;
        [SerializeField] private GameObject _misile;
        [SerializeField] private GameObject _minePrefab;
        [SerializeField] private GameObject _shotSeveralBullets;
        [SerializeField] private GameObject _shotMissile;
        [SerializeField] private GameObject _shotOneBullet;

        public override void InstallBindings()
        {
            InstallFactories();
            DeclareSignals();
        }

        private void DeclareSignals()
        {
            Container.DeclareSignal<SpawnNewPowerUpSignal>();
            Container.DeclareSignal<SpawnNewProjectileSignal>();
            Container.DeclareSignal<SpawnNewMineSignal>();
            Container.DeclareSignal<EnemyDestroyedByTimeSignal>();
            
            
        }

        private void InstallFactories()
        {
            Container.BindFactory<ProjectileInfo, BulletTypes, Bullet, Bullet.Factory>()
                .FromPoolableMemoryPool<ProjectileInfo, BulletTypes, Bullet, BulletPool>(poolBinder =>
                    poolBinder
                        .WithInitialSize(50)
                        .FromComponentInNewPrefab(_bulletPrefab)
                        .WithGameObjectName("Bullet")
                        .UnderTransformGroup("Bullets"));


            Container.BindFactory<PowerUpInfo, PowerUp, PowerUp.Factory>()
                .FromPoolableMemoryPool<PowerUpInfo, PowerUp, PowerUpPool>(poolBinder => poolBinder
                    .WithInitialSize(10)
                    .FromComponentInNewPrefab(_powerUpPrefab)
                    .WithGameObjectName("PowerUp")
                    .UnderTransformGroup("PowerUps"));

            Container.BindFactory<EnemyInfo, IShot, Enemy1, Enemy1.Factory>()
                .FromPoolableMemoryPool<EnemyInfo, IShot, Enemy1, Enemy1Pool>(poolBinder =>
                    poolBinder
                        .WithInitialSize(10)
                        .FromComponentInNewPrefab(_enemy1Prefab)
                        .WithGameObjectName("Enemy1")
                        .UnderTransformGroup("Enemy"));

            Container.BindFactory<EnemyInfo, IShot, Enemy2, Enemy2.Factory>()
                .FromPoolableMemoryPool<EnemyInfo, IShot, Enemy2, Enemy2Pool>(poolBinder =>
                    poolBinder
                        .WithInitialSize(10)
                        .FromComponentInNewPrefab(_enemy2Prefab)
                        .WithGameObjectName("Enemy2")
                        .UnderTransformGroup("Enemy"));

            Container.BindFactory<EnemyInfo, IShot, Enemy3, Enemy3.Factory>()
                .FromPoolableMemoryPool<EnemyInfo, IShot, Enemy3, Enemy3Pool>(poolBinder =>
                    poolBinder
                        .WithInitialSize(10)
                        .FromComponentInNewPrefab(_enemy3Prefab)
                        .WithGameObjectName("Enemy3")
                        .UnderTransformGroup("Enemy"));

            Container.BindFactory<EnemyInfo, IShot, Enemy, CustomEnemyFactory.EnemyFactory>()
                .FromFactory<CustomEnemyFactory>();


            Container.BindFactory<ObstacleInfo, Meteor, Meteor.Factory>()
                .FromPoolableMemoryPool<ObstacleInfo, Meteor, Obstacle1Pool>(poolBinder =>
                    poolBinder
                        .WithInitialSize(10)
                        .FromComponentInNewPrefab(_obstacle1Prefab)
                        .WithGameObjectName("Obstacle1")
                        .UnderTransformGroup("Obstacles"));


            Container.BindFactory<MineInfo, Mine, Mine.Factory>()
                .FromPoolableMemoryPool<MineInfo, Mine, MinePool>(poolBinder =>
                    poolBinder
                        .WithInitialSize(10)
                        .FromComponentInNewPrefab(_minePrefab)
                        .WithGameObjectName("Mine")
                        .UnderTransformGroup("Obstacles"));

            Container.BindFactory<ProjectileInfo, Missile, Missile.Factory>()
                .FromPoolableMemoryPool<ProjectileInfo, Missile, MisilePool>(poolBinder =>
                    poolBinder
                        .WithInitialSize(10)
                        .FromComponentInNewPrefab(_misile)
                        .WithGameObjectName("Misile")
                        .UnderTransformGroup("Misiles"));


            Container
                .BindFactory<ObstacleInfo, ObstacleType, SpaceObstacle, CustomObstacleFactory.ObstacleFactory
                >()
                .FromFactory<CustomObstacleFactory>();


            Container.BindFactory<int, float, int, EndGameMenuView, EndGameMenuView.Factory>()
                .FromPoolableMemoryPool<int, float, int, EndGameMenuView, UIPool>(poolBinder => poolBinder
                    .WithInitialSize(1)
                    .FromComponentInNewPrefab(_endGamePrefab)
                    .WithGameObjectName("EndGameView")
                    .UnderTransformGroup("UI"));


            Container.BindFactory<ShotSeveralBullets, ShotSeveralBullets.Factory>()
                .FromPoolableMemoryPool<ShotSeveralBullets, IShotSeveralBulletsPool>(poolBinder =>
                    poolBinder
                        .WithInitialSize(50)
                        .FromComponentInNewPrefab(_shotSeveralBullets)
                        .WithGameObjectName("ShotMissileBullets")
                        .UnderTransformGroup("StrategiesShooting"));

            Container.BindFactory<ShotMissile, ShotMissile.Factory>()
                .FromPoolableMemoryPool<ShotMissile, IShotMissilePool>(poolBinder =>
                    poolBinder
                        .WithInitialSize(50)
                        .FromComponentInNewPrefab(_shotMissile)
                        .WithGameObjectName("ShotMissile")
                        .UnderTransformGroup("StrategiesShooting"));

            Container.BindFactory<ShotOneBullet, ShotOneBullet.Factory>()
                .FromPoolableMemoryPool<ShotOneBullet, IShotOneBulletPool>(poolBinder =>
                    poolBinder
                        .WithInitialSize(50)
                        .FromComponentInNewPrefab(_shotOneBullet)
                        .WithGameObjectName("ShotOneBullet")
                        .UnderTransformGroup("StrategiesShooting"));
        }

        class IShotSeveralBulletsPool : MonoPoolableMemoryPool<IMemoryPool, ShotSeveralBullets>
        {
        }

        class IShotOneBulletPool : MonoPoolableMemoryPool<IMemoryPool, ShotOneBullet>
        {
        }

        class IShotMissilePool : MonoPoolableMemoryPool<IMemoryPool, ShotMissile>
        {
        }

        class BulletPool : MonoPoolableMemoryPool<ProjectileInfo, BulletTypes, IMemoryPool, Bullet>
        {
        }

        class UIPool : MonoPoolableMemoryPool<int, float, int, IMemoryPool, EndGameMenuView>
        {
        }

        // class EnemyPool : MonoPoolableMemoryPool<float, float, float, float, EnemyType, IMemoryPool, Enemy>
        // {
        // }
        class Enemy1Pool : MonoPoolableMemoryPool<EnemyInfo, IShot, IMemoryPool, Enemy1>
        {
        }

        class Enemy2Pool : MonoPoolableMemoryPool<EnemyInfo, IShot, IMemoryPool, Enemy2>
        {
        }

        class Enemy3Pool : MonoPoolableMemoryPool<EnemyInfo, IShot, IMemoryPool, Enemy3>
        {
        }

        class Obstacle1Pool : MonoPoolableMemoryPool<ObstacleInfo, IMemoryPool, Meteor>
        {
        }

        class MinePool : MonoPoolableMemoryPool<MineInfo, IMemoryPool, Mine>
        {
        }


        class MisilePool : MonoPoolableMemoryPool<ProjectileInfo, IMemoryPool, Missile>
        {
        }

        // class Enemy3Pool : MonoPoolableMemoryPool<float, int, float, float, EnemyType, IMemoryPool, Enemy2>
        // {
        // }


        class PowerUpPool : MonoPoolableMemoryPool<PowerUpInfo, IMemoryPool, PowerUp>
        {
        }
    }
}