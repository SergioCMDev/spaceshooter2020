﻿using System;
using Zenject;

namespace Code
{
    public class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller>
    {
        public PlayerSettings Player;
        public EnemySettings Enemy;
        public SpaceObstaclesSettings SpaceObstacles;
        public PowerUpsSettings PowerUps;

        [Serializable]
        public class PlayerSettings
        {
            public int BaseMovementSpeed = 13;
            public int BaseBulletsDamage = 5;
            public int BaseLife = 50;
            public float BaseMultipleShotsTime = 0;
            public float BaseShieldsTime = 0;
            public float BaseBulletsSpeed = 16;
            public float PlayerBulletsMinDamageToChangeToRedSprite = 10;
            public float PlayerBulletsMinDamageToChangeToOrangeSprite = 20;
        }
    }

    [Serializable]
    public class Enemy1Settings
    {
        public int MovementSpeedBase = 4;
        public int ScoreForDestroyEnemy = 30;
        public int BulletDamage = 4;
        public int Life = 10;
        public int TimeBetweenShots = 1;
        public float BulletSpeed = 10;
        public float LifeTime = 10;
        public int TimeToStartShooting = 2;
    }

    [Serializable]
    public class Enemy2Settings
    {
        public int MovementSpeedBase = 8;
        public int ScoreForDestroyEnemy = 50;
        public int BulletDamage = 8;
        public int BulletSpeed = 15;
        public int Life = 20;
        public int TimeBetweenShots = 3;
        public float LifeTime = 20;
        public int TimeToStartShooting = 4;
    }

    [Serializable]
    public class Enemy3Settings
    {
        public MissilSeetings Missil;
        public int MovementSpeedBase;
        public int ScoreForDestroyEnemy;
        public int Life = 20;
        public int TimeBetweenShots = 5;

        public float LifeTime = 15;
        public int TimeToStartShooting = 5;
    }

    [Serializable]
    public class MissilSeetings
    {
        public int Life;
        public int LifeTime;
        public int Damage;
        public int Speed;
    }

    [Serializable]
    public class EnemySettings
    {
        public Enemy3Settings Enemy3Settings;
        public Enemy2Settings Enemy2Settings;
        public Enemy1Settings Enemy1Settings;

        public int MinProbabiltyToSpawnEnemy1 = 0;
        public int MaxProbabiltyToSpawnEnemy1 = 30;
        public int MinProbabiltyToSpawnEnemy2 = 30;
        public int MaxProbabiltyToSpawnEnemy2 = 80;
        public int MinProbabiltyToSpawnEnemy3 = 80;
        public int MaxProbabiltyToSpawnEnemy3 = 100;
        public int DamageByHit = 15;
        public int TimeToSpawnEnemy = 2;
        public int MaxEnemiesToSpawn = 10;
        public int PercentageToGeneratePowerUps = 40;
    }

    [Serializable]
    public class SpaceObstaclesSettings
    {
        public int ScoreForDestroyMeteors;
        public int ScoreForDestroyMines;
        public int MineDamage = 30;
        public int MeteorDamage;
        public float MeteorSpeed;
        public float MineSpeed = 5;
        public float MeteorLife;
        public float MeteorLifeTime;
        public int MinProbabilityToSpawnMeteors = 0;
        public int MaxProbabilityToSpawnMeteors = 30;
        public int MinProbabilityToSpawnMine = 30;
        public int MaxProbabilityToSpawnMine = 80;
        public int MineLife = 2;
        public int MineLifeTime = 20;
        public int MaxObstaclesToSpawn = 10;
        public float TimeToStartSpawningObstacles = 1;
        public float TimeToSpawnObstaclesOnceStarts = 3;
    }

    [Serializable]
    public class PowerUpsSettings
    {
        public PowerUpBulletSpeedSettings PowerUpBulletSpeed;
        public PowerUpBulletDamageSettings PowerUpBulletDamage;
        public PowerUpShieldSettings PowerUpShield;
        public PowerUpMovementSpeedSettings PowerUpMovementSpeed;
        public PowerUpMultipleShotSettings PowerUpMultipleShot;

        public int MinProbabiltyToSpawnMovementSpeed = 0;
        public int MaxProbabiltyToSpawnMovementSpeed = 30;

        public int MinProbabiltyToSpawnBulletDamage = 30;
        public int MaxProbabiltyToSpawnBulletDamage = 50;

        public int MinProbabiltyToSpawnBulletSpeed = 80;
        public int MaxProbabiltyToSpawnBulletSpeed = 100;


        public int MinProbabiltyToSpawnShield = 50;
        public int MaxProbabiltyToSpawnShield = 60;


        public int MinProbabiltyToSpawnMultipleShot = 60;
        public int MaxProbabiltyToSpawnMultipleShot = 80;
    }

    [Serializable]
    public class PowerUpBulletSpeedSettings
    {
        public float LifeTimeAtSpace = 10;
        public float LifeTimeAtPlayer = 5;
        public float ImproveValue = 5;
    }

    [Serializable]
    public class PowerUpBulletDamageSettings
    {
        public float LifeTimeAtSpace = 10;
        public float LifeTimeAtPlayer = 5;
        public float ImproveValue = 5;
    }

    [Serializable]
    public class PowerUpShieldSettings
    {
        public float LifeTimeAtSpace = 10;
        public float LifeTimeAtPlayer = 5;
        public float ImproveValue = 5;
    }

    [Serializable]
    public class PowerUpMovementSpeedSettings
    {
        public float LifeTimeAtSpace = 10;
        public float LifeTimeAtPlayer = 5;
        public float ImproveValue = 5;
    }

    [Serializable]
    public class PowerUpMultipleShotSettings
    {
        public float LifeTimeAtSpace = 10;
        public float LifeTimeAtPlayer = 5;
        public float ImproveValue = 5;
    }
}